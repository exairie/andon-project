// Rev 2 11 Feb 2020 jam 21:35
// ganti akun firebase
// Rev 1 31 jan 2020 jam 22:44
#include <PZEM004Tv30.h>
#include <SoftwareSerial.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SimpleTimer.h>

#define MONITOR_INTERVAL 15000

SimpleTimer timer;
PZEM004Tv30 pzem_r(&Serial1);
PZEM004Tv30 pzem_s(53, 52);
PZEM004Tv30 pzem_t(51, 50);

//variabel parsing data
String dataIn = "";

// sensor diletakkan di pin 2
#define ONE_WIRE_BUS 2
// setup sensor
OneWire oneWire(ONE_WIRE_BUS); 
// berikan nama variabel,masukkan ke pustaka Dallas
DallasTemperature sensortemp(&oneWire);

int ledNumber;
int ledRelay[4] = {38, 40, 42, 44};

bool disconnectState = false;
bool disconnectSwitch = false;

bool wifiDisconnectState = false;
bool wifiDisconnectSwitch = false;

bool stringComplete = false;
bool monitoringStatus = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
//  Serial.setTimeout(10);
  Serial3.begin(9600);

  for (int i = 0; i < 4; i++) {
    pinMode(ledRelay[i], OUTPUT);
  }

  for (ledNumber = 0; ledNumber < 4; ledNumber++) {
    digitalWrite(ledRelay[ledNumber], LOW);
    delay(100);
  }
  for (ledNumber = 0; ledNumber < 4; ledNumber++) {
    digitalWrite(ledRelay[ledNumber], HIGH);
    delay(100);
  }
  
//  Serial3.setTimeout(10);

  

//  Serial3.println("HANDSHAKE");
  
//  timer.setInterval(MONITOR_INTERVAL, checkMonitoringValue);
  Serial.println("Setting up monitoring interval to " + String(MONITOR_INTERVAL) + " msecs");

  timer.setInterval(250, blinkWifikDisconnect);
  timer.setInterval(1000, blinkServerkDisconnect);
  timer.setInterval(5000, sendMonitoringValueAutoLoop);
}

void checkMonitoringValue(){
  if(disconnectState){
    Serial.println("Disconnected. Will not send any monitoring value");
  }
  Serial.println("Updating Monitoring Value");
  digitalWrite(ledRelay[2],LOW);
  meterMonitoring();
  digitalWrite(ledRelay[2],HIGH);
}

/** DEBUG : Click relay if disconnected from server */
void blinkServerkDisconnect(){
  if(disconnectState){
    digitalWrite(ledRelay[0],disconnectSwitch ? LOW : HIGH);
    disconnectSwitch = !disconnectSwitch;
  }else{
    disconnectSwitch = false;
    digitalWrite(ledRelay[0],LOW);
  }
}

/** DEBUG : Click relay if disconnected from wifi*/
void blinkWifikDisconnect(){
  if(wifiDisconnectState){
    digitalWrite(ledRelay[1],wifiDisconnectSwitch ? LOW : HIGH);
    wifiDisconnectSwitch = !wifiDisconnectSwitch;
  }else{
    wifiDisconnectSwitch = false;
    digitalWrite(ledRelay[1],LOW);
  }
}

void loop() {  
  timer.run();  
  
  if (Serial3.available() > 0)
  {    
    String msg = Serial3.readStringUntil('\r');
//    while(Serial3.available()){
//      char c = (char)Serial3.read();
//      msg += c;
//      Serial.print(c);      
//    }

    if(msg.indexOf("SVRCON") >= 0){
      Serial.println("Server is Connected");
      disconnectState = false;
    }
    if(msg.indexOf("SVRDIS") >= 0){
      Serial.println("Server is Disonnected");
      disconnectState = true;
    }
    if(msg.indexOf("WIFCON") >= 0){
      Serial.println("WIFI is Connected");
      wifiDisconnectSwitch = false;
    }
    if(msg.indexOf("WIFDIS") >= 0){
      Serial.println("WIFI is Disconected");
      wifiDisconnectSwitch = true;
    }

    dataIn = msg;
    ledControlling();
  }
  if(Serial.available()){
    String s = Serial.readStringUntil('\r');
    Serial3.println(s);
    if(s.indexOf("START") >= 0){
      monitoringStatus = true;
    }
    if(s.indexOf("STOP") >= 0){
      monitoringStatus = false;
    }
  }
}

//Ketika data masuk melalui serial 3
void serialEvent3() {
//  monitoringStatus = false;
//  while (Serial3.available() > 0) {
//    // get the new byte:
//    char inChar = (char)Serial3.read();
//    // add it to the dataIn:
//    dataIn += inChar;
//    // if the incoming character is a newline, set a flag so the main loop can
//    // do something about it:
//    if (inChar == '\n') {
//      stringComplete = true;
//    }
//
//    ledControlling();
//
//    if (stringComplete) {
//      // clear the string:
//      // Serial.println(dataIn);
//      monitoringStatus = true;
//      dataIn = "";
//      stringComplete = false;
//    }
//  }
}
