void ledControlling() {
  for (ledNumber = 0; ledNumber < 4; ledNumber++) {
    if (dataIn == "L" + String(ledNumber) + "A") {
      Serial.println("Led " + String(ledNumber) + " Aktif");
      digitalWrite(ledRelay[ledNumber], LOW);
    }
    else if (dataIn == "L" + String(ledNumber) + "D") {
      Serial.println("Led " + String(ledNumber) + " mati");
      digitalWrite(ledRelay[ledNumber], HIGH);
    }
  }
}
