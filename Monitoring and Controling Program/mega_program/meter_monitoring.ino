const int MonitoringParameterCount = 17;
int monitoringParameterCursor = -1;
int dataLength = 53;
byte byteData[54];
String MonitoringLabel[] = {
    "Voltage R : ",
    "Voltage S : ",
    "Voltage T : ",
    "Voltage Jumlah : ",
    "Current R : ",
    "Current S : ",
    "Current T : ",
    "Current Jumlah : ",
    "Power R : ",
    "Power S : ",
    "Power T : ",
    "Power Jumlah : ",
    "Energy Jumlah : ",
    "Frequency Jumlah : ",
    "Kenaikantemperature A : ",
    "Kenaikantemperature B : ",
    "Vibrationsensor A : "
};

float MonitoringValue [] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

void packMonitoringValue(){  
  /** Header Byte, 11111111 */  
  byteData[0] = 0xff;
  /** Trailer Byte, 10101010*/
  byteData[dataLength - 1] = 0xaa;

  const int offset = 1;

  Serial.print(byteData[0],HEX);

  for(int i = 0; i < MonitoringParameterCount;i++){
    long mv = (long)(MonitoringValue[i] * 100);
//    Serial.print(MonitoringLabel[i] + " = " + String(mv) + "\r\n");
    // change value into 3-byte data
    byteData[offset + (3 * i)] = (byte)((mv >> 16) & 0xff);
    byteData[offset + (3 * i) + 1] = (byte)((mv >> 8) & 0xff);
    byteData[offset + (3 * i) + 2] = (byte)((mv >> 0) & 0xff);
    Serial.print(" ");
    Serial.print(byteData[offset + (3 * i)],HEX);
    Serial.print(" ");
    Serial.print(byteData[offset + (3 * i) + 1],HEX);
    Serial.print(" ");
    Serial.print(byteData[offset + (3 * i) + 2],HEX);
  }
  Serial.print(" ");
  Serial.println(byteData[dataLength - 1],HEX);

  return byteData;
}

void sendMonitoringValueAutoLoop(){ 
  digitalWrite(ledRelay[2],LOW);  
  Serial.println("Getting Values");
  meterMonitoring();
  Serial.println("Populating Values");
  packMonitoringValue();
  Serial.println("Sending");
  for(int i = 0;i < dataLength;i++){
    Serial3.write(byteData[i]);
  }
  Serial.println();
  Serial.println("Done");
  
  digitalWrite(ledRelay[2],HIGH);
  return;
  
  if(monitoringParameterCursor >=0 && monitoringParameterCursor < MonitoringParameterCount){
    sendMonitoringValue(MonitoringValue[monitoringParameterCursor], MonitoringLabel[monitoringParameterCursor]);
    monitoringParameterCursor++;
  }else if(monitoringParameterCursor >= MonitoringParameterCount){
    monitoringParameterCursor = -1;
  }
}
void sendMonitoringValue(float floatValue, String meterSelector)
{
  if (floatValue != NAN)
  {
    //    Serial.print(meterSelector);
    //    Serial.print(floatValue);
    //    Serial.println(meterUnit);
    // Send Data
    String payload = "=="+meterSelector + String(floatValue);
    Serial3.println(payload);
    Serial.println(payload);
  }
  else
  {
    Serial.println("Error reading device");
  }
}
void setMonitoringValue(float value, int index){
  if(isnan(value)) value = -1;
  MonitoringValue[index] = value;
}
void meterMonitoring()
{
  float voltage_R = pzem_r.voltage();
  float current_R = pzem_r.current();
  float power_R = pzem_r.power();
  float energy_R = pzem_r.energy();
  float frequency_R = pzem_r.frequency();

  float voltage_S = pzem_s.voltage();
  float current_S = pzem_s.current();
  float power_S = pzem_s.power();
  float energy_S = pzem_s.energy();
  float frequency_S = pzem_s.frequency();

  float voltage_T = pzem_t.voltage();
  float current_T = pzem_t.current();
  float power_T = pzem_t.power();
  float energy_T = pzem_t.energy();
  float frequency_T = pzem_t.frequency();

  float voltage_total = ((voltage_R + voltage_S + voltage_T) / 3) * 1.732;
  float current_total = current_R + current_S + current_T;
  float power_total = power_R + power_S + power_T;
  float energy_total = energy_R + energy_S + energy_T;

  /** Test */
//  voltage_R = 11;
//  voltage_S = 10;
//  voltage_T = 1024;
  
  setMonitoringValue(voltage_R, 0); 
  setMonitoringValue(voltage_S, 1); 
  setMonitoringValue(voltage_T, 2);  
  setMonitoringValue(voltage_total, 3);  

  setMonitoringValue(current_R, 4);  
  setMonitoringValue(current_S, 5);  
  setMonitoringValue(current_T, 6);
  
  setMonitoringValue(current_total, 7);  

  setMonitoringValue(power_R, 8);  
  setMonitoringValue(power_S, 9);  
  setMonitoringValue(power_T, 10);  
  setMonitoringValue(power_total, 11);
  
  setMonitoringValue(energy_total, 12);  

  setMonitoringValue(frequency_R, 13);
  
  sensortemp.requestTemperatures();
  float temperature_1 = sensortemp.getTempCByIndex(0);
  float temperature_2 = sensortemp.getTempCByIndex(1);
  
  setMonitoringValue(temperature_1, 14);  
  setMonitoringValue(temperature_2, 15);
  

  float vibration1 = 7.6;
  setMonitoringValue(vibration1, 16);
  
  /** Set parameter cursor to 0 to enable auto loop */
  monitoringParameterCursor = 0;

  return;
//  
//  sendMonitoringValue(voltage_R, "Voltage R : ", " V");
//  sendMonitoringValue(voltage_S, "Voltage S : ", " V"); 
//  sendMonitoringValue(voltage_T, "Voltage T : ", " V");  
//  sendMonitoringValue(voltage_total, "Voltage Jumlah : ", " V");  
//
//  sendMonitoringValue(current_R, "Current R : ", " A");  
//  sendMonitoringValue(current_S, "Current S : ", " A");  
//  sendMonitoringValue(current_T, "Current T : ", " A");
//  
//  sendMonitoringValue(current_total, "Current Jumlah : ", " A");  
//
//  sendMonitoringValue(power_R, "Power R : ", " W");  
//  sendMonitoringValue(power_S, "Power S : ", " W");  
//  sendMonitoringValue(power_T, "Power T : ", " W");  
//  sendMonitoringValue(power_total, "Power Jumlah : ", " W");
//  
//  sendMonitoringValue(energy_total, "Energy Jumlah : ", " KWH");  
//
//  sendMonitoringValue(frequency_R, "Frequency Jumlah : ", " Hz");
//  
//  sensortemp.requestTemperatures();
//  float temperature_1 = sensortemp.getTempCByIndex(0);
//  float temperature_2 = sensortemp.getTempCByIndex(1);
//  
//  sendMonitoringValue(temperature_1, "Kenaikantemperature A : ", " Celcius");  
//  sendMonitoringValue(temperature_2, "Kenaikantemperature B : ", " Celcius");
//  
//
//  float vibration1 = 7.6;
//  sendMonitoringValue(vibration1, "Vibrationsensor A : ", " mm/s ");
  

  
}
