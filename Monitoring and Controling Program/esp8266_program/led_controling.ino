void ledControl() {
  String ledParameter = "";
  ledNumber = 0;
  for (ledNumber = 0; ledNumber < 4; ledNumber++) {

    if (Firebase.getBool("/"+userId+"/panel/ledControls/" + String(ledNumber) + "/status") == true)
    {
      if (booleanLedStatus[ledNumber] != 1) {
        delay(50);
        Serial.println("L" + String(ledNumber) + "A");
        delay(50);
        booleanLedStatus[ledNumber] = 1;
      }
    }

    else if (Firebase.getBool("/"+userId+"/panel/ledControls/" + String(ledNumber) + "/status") == false)
    {
      if (booleanLedStatus[ledNumber] != 0) {
        delay(50);
        Serial.println("L" + String(ledNumber) + "D");
        delay(50);
        booleanLedStatus[ledNumber] = 0;
      }
    }

    if (Firebase.failed()) // Check for errors
    {
      Serial.print("setting /number failed:");

      Serial.println(Firebase.error());

      return;
    }
  }
}
