void parsingData() {

  //kirim data yang telah diterima sebelumnya
  //  Serial.print("data masuk : ");
  //  Serial.print(dataIn);

  //proses parsing data
  meterMonitoring('V', 'R', "Voltage");
  meterMonitoring('C', 'R', "Current");
  meterMonitoring('P', 'R', "Power");
  
  meterMonitoring('V', 'S', "Voltage");
  meterMonitoring('C', 'S', "Current");
  meterMonitoring('P', 'S', "Power");

  meterMonitoring('V', 'T', "Voltage");
  meterMonitoring('C', 'T', "Current");
  meterMonitoring('P', 'T', "Power");

  meterMonitoring('V', 'J', "Voltage");
  meterMonitoring('C', 'J', "Current");
  meterMonitoring('P', 'J', "Power");
  meterMonitoring('E', 'J', "Energy");
  meterMonitoring('F', 'J', "Frequency");

  meterMonitoring('K', 'A', "Temperature");
  meterMonitoring('K', 'B', "Temperature");

  meterMonitoring('V', 'A', "Vibrasi");


}
