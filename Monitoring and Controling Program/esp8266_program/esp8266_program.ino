// Rev 2 11 Feb 2020 jam 21:35 --> ganti akun firebase
// Rev 1 31 Jan 2020 jam 22:44
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

// Set these to run connect to database and wifi
#define FIREBASE_HOST "web-app-integra-41cb3.firebaseio.com"
#define FIREBASE_AUTH "vuZ4kuLrz6GCyQAzQ8nTGrCRYvAS0G9j0b8QWkoS"
#define WIFI_SSID "PANCA"
#define WIFI_PASSWORD "panca2378"

//variabel parsing data
String dataIn;
int panjangData;
int panjangData2;
int panjangData3;
int panjangData4;
int ledNumber;
int booleanLedStatus[4];

boolean parsing = false;
boolean sendDataSequence = true;

//Parameter Parse
String userId = "7cRbiwcrFwQn0gj6Cki8YYh3wbH3"; // untuk user name #01
//String userId = "9J6vCotbxPgkevwZ1qiphiDMK722"; // untuk user name #02
String dataStringIn;
float dataInFloat;

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(10);

  //Parsing Data
  dataIn = "";

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.stream("/" + userId + "/panel/ledControls");

  //check status LED
  for (ledNumber = 0; ledNumber < 4; ledNumber++) {

    if (Firebase.getBool("/" + userId + "/panel/ledControls/" + String(ledNumber) + "/status") == true) {
      booleanLedStatus[ledNumber] = 1;
      Serial.println("L" + String(ledNumber) + "A");
      delay(50);
    }

    else if (Firebase.getBool("/" + userId + "/panel/ledControls/" + String(ledNumber) + "/status") == false) {
      booleanLedStatus[ledNumber] = 0;
      Serial.println("L" + String(ledNumber) + "D");
      delay(50);

    }
  }
}

void loop() {
    if (Firebase.available()) {
      FirebaseObject event = Firebase.readEvent();
      String eventType = event.getString("type");
      eventType.toLowerCase();
      if (eventType == "patch") {
        ledControl();
      }
    }
  
    else {
      if (Serial.available() > 0) {
        char inChar = (char)Serial.read();
        dataIn += inChar;
        if (inChar == '\n') {
          parsing = true;
        }
      }
  
      if (parsing) {
        parsingData();
        parsing = false;
        dataIn = "";
      }
    }

}
