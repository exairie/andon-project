
#include <DallasTemperature.h>
#include "libs.h"
void startupTemp(DallasTemperature sensors)
{
    sensors.begin();

    // locate devices on the bus
    Serial.println("Locating devices...");
    Serial.print("Found ");
    int deviceCount = sensors.getDeviceCount();
    Serial.print(deviceCount, DEC);
    Serial.println(" devices.");
    Serial.println("");

    Serial.println("Printing addresses...");
    for (int i = 0; i < deviceCount; i++)
    {
        DeviceAddress Thermometer;
        Serial.print("Sensor ");
        Serial.print(i + 1);
        Serial.print(" : ");
        sensors.getAddress(Thermometer, i);
        printAddress(Thermometer);
    }
}
void printAddress(DeviceAddress deviceAddress)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        Serial.print("0x");
        if (deviceAddress[i] < 0x10)
            Serial.print("0");
        Serial.print(deviceAddress[i], HEX);
        if (i < 7)
            Serial.print(", ");
    }
    Serial.println("");
}