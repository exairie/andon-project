#include <WiFiEsp.h>
#include <WiFiEspClient.h>

//#include <OneWire.h>
//#include <DallasTemperature.h>
//#include <PZEM004Tv30.h>

#define mSerial Serial3
#define ONE_WIRE_BUS 2


//PZEM004Tv30 pzem_r(&Serial1);
//PZEM004Tv30 pzem_s(53, 52);
//PZEM004Tv30 pzem_t(51, 50);
WiFiEspClient client;


// sensor diletakkan di pin 2
// setup sensor
//OneWire oneWire(ONE_WIRE_BUS); 
// berikan nama variabel,masukkan ke pustaka Dallas
//DallasTemperature sensortemp(&oneWire);


const char* ssid = "EXPC";
const char* pass = "12345678";
int status; 

const uint16_t port = 7888;
const char* host = "datditdut.com"; // IP serveur - Server IP


int ledNumber;
int ledRelay[4] = {38, 40, 42, 44};

bool stringComplete = false;
bool monitoringStatus = false;
//variabel parsing data
String dataIn = "";
  
void setup() {
  
    // put your setup code here, to run once:
  Serial.begin(9600);
  mSerial.begin(115200);
//  mSerial.println("AT+UART_DEF=115200,8,1,0,0\r\n");
//  mSerial.println("AT+UART_DEF=9600,8,1,0,0\r\n");
//  mSerial.println("AT+UART_DEF=9600,8,1,0,0\r\n");
//  mSerial.println("AT+UART_DEF=9600,8,1,0,0\r\n");
//  mSerial.println("AT+UART_DEF=9600,8,1,0,0\r\n");
//  mSerial.begin(9600);
  Serial.println("Test");
  WiFi.init(&mSerial);  
  while ( status != WL_CONNECTED) {
    Serial.println("Trying to connect...");    
    status = WiFi.begin(ssid, pass);
  }  
  Serial.print("Connected to ");
  Serial.println(ssid);

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // put your main code here, to run repeatedly:
//  Serial.println(client.available());
  if(Serial.available()){
    while(Serial.available()){
      char c = Serial.read();
      Serial.print(c);
      client.print(c);
    }
    client.println();
  }
  if(client.available()){
    while(client.available()){
      Serial.print((char)client.read());
    }   
    client.println("COMMAND RECEIVED!");
  }
  if (!client.connected())
  {
    byte mac[6];
    WiFi.macAddress(mac);
    
    Serial.println("Trying to connect to server " + String(host) + ":" + String(port));
    
    char buf[20];
    sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);

    Serial.println("Registering Mac Address " + String(buf));

    
    client.connect(host, port);
    client.println("HANDSHAKE::OK");    
    
    client.println("MCA::" + String(buf));
//     Serial.println("Status " + client.connected() ? "CONNECTED" : "NOT CONNECTED");
    Serial.println("Connect OK. Ready");         
  }
  
  
}
