#ifdef ENABLE
void sendMonitoringValue(float floatValue, String meterSelector, String meterUnit)
{
  if (floatValue != NAN)
  {
    //    Serial.print(meterSelector);
    //    Serial.print(floatValue);
    //    Serial.println(meterUnit);
    // Send Data
//    client.println(meterSelector + "::"+String(floatValue)+"="+meterUnit);
  }
  else
  {
    Serial.println("Error reading device");
  }
}
void meterMonitoring()
{
  int d = 250;
  float voltage_R = pzem_r.voltage();
  float current_R = pzem_r.current();
  float power_R = pzem_r.power();
  float energy_R = pzem_r.energy();
  float frequency_R = pzem_r.frequency();

  float voltage_S = pzem_s.voltage();
  float current_S = pzem_s.current();
  float power_S = pzem_s.power();
  float energy_S = pzem_s.energy();
  float frequency_S = pzem_s.frequency();

  float voltage_T = pzem_t.voltage();
  float current_T = pzem_t.current();
  float power_T = pzem_t.power();
  float energy_T = pzem_t.energy();
  float frequency_T = pzem_t.frequency();

  float voltage_total = ((voltage_R + voltage_S + voltage_T) / 3) * 1.732;
  float current_total = current_R + current_S + current_T;
  float power_total = power_R + power_S + power_T;
  float energy_total = energy_R + energy_S + energy_T;

  sendMonitoringValue(voltage_R, "Voltage R : ", " V");
  delay(d);
  sendMonitoringValue(voltage_S, "Voltage S : ", " V");
  delay(d);
  sendMonitoringValue(voltage_T, "Voltage T : ", " V");
  delay(d);
  sendMonitoringValue(voltage_total, "Voltage Jumlah : ", " V");
  delay(d);

  sendMonitoringValue(current_R, "Current R : ", " A");
  delay(d);
  sendMonitoringValue(current_S, "Current S : ", " A");
  delay(d);
  sendMonitoringValue(current_T, "Current T : ", " A");
  delay(d);
  sendMonitoringValue(current_total, "Current Jumlah : ", " A");
  delay(d);

  sendMonitoringValue(power_R, "Power R : ", " W");
  delay(d);
  sendMonitoringValue(power_S, "Power S : ", " W");
  delay(d);
  sendMonitoringValue(power_T, "Power T : ", " W");
  delay(d);
  sendMonitoringValue(power_total, "Power Jumlah : ", " W");
  delay(d);

  sendMonitoringValue(energy_total, "Energy Jumlah : ", " KWH");
  delay(d);

  sendMonitoringValue(frequency_R, "Frequency Jumlah : ", " Hz");
  delay(d);

  sensortemp.requestTemperatures();
  float temperature_1 = sensortemp.getTempCByIndex(0);
  float temperature_2 = sensortemp.getTempCByIndex(1);
  
  sendMonitoringValue(temperature_1, "Kenaikantemperature A : ", " Celcius");
  delay(1500);

  sendMonitoringValue(temperature_2, "Kenaikantemperature B : ", " Celcius");
  delay(d);

  float vibration1 = 7.6;
  sendMonitoringValue(vibration1, "Vibrationsensor A : ", " mm/s ");
  delay(d);

  
}
#endif
