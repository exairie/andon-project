#include <ESP8266WiFi.h>
#include <SimpleTimer.h>
// WIFI
const char* ssid = "VIRUS DIGITAL INDONESIA";     // SSID
const char* password = "23456789"; // Password

WiFiClient client;
const uint16_t port = 7888;
const char* host = "datditdut.com"; // IP serveur - Server IP
short loader = 0;
bool disconnectState = true;
String lastData;

bool initOk = false;

long lastServerCheck = millis();

SimpleTimer timer;

void setup() {
  // put your setup code here, to run once:
// connect to WIFI
  Serial.begin(9600);
  Serial.println("ESP INIT....");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  Serial.println("WIFDIS");
  Serial.println("SVRDIS");
  while (WiFi.status() != WL_CONNECTED)
  {    
    Serial.print(".");
    delay (100);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  delay(2000);  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("MCA: ");
  Serial.println(WiFi.macAddress());
  Serial.println("WIFCON");

  timer.setInterval(1000, updateServerState);
  timer.setInterval(1000, checkServerConnection);

}
void checkServerConnection(){
  /** Check if last server ping is more than 30secs */
  if(millis() - lastServerCheck > 30000){
    Serial.println("NO Server Check transmission during the last 30 secs");
    Serial.println("Assuming connectin failure, restarting device");
    Serial.println("ESPRESTART");
    ESP.restart();
  }
}
void sendData(String data){
  lastData = data;
  client.println(data);
}
void updateServerState(){
  if(client.connected()){    
    Serial.println("SVRCON");
  }else{    
    Serial.println("SVRDIS");
  }
}
void loop() {
  timer.run();
  // put your main code here, to run repeatedly:
  if(WiFi.status() != WL_CONNECTED){
    Serial.println("WIFDIS");
    while (WiFi.status() != WL_CONNECTED)
    {    
      Serial.print(".");
      delay (100);
    }
    Serial.println("WIFCON");
  }
  if (!client.connected())
  {    
    if(initOk){
      // Previous initialization was done. this is a disconnection events. Restart SDK
      Serial.println("Rebooting due to failed Connection");
      ESP.restart();
    }
    disconnectState = true;
    client.stop();
    Serial.println("SVRDIS");
    Serial.println("Trying to connect to server " + String(host) + ":" + String(port));
    client.connect(host, port);
    client.println("==HANDSHAKE::OK");
    client.println("==MCA::" + String(WiFi.macAddress()));
    initOk = client.connected();
    return;
  }
  else
  {
    if(disconnectState){
      Serial.println("SVRCON");
      disconnectState = false;
    }
    //blinkLedConst();
  }

  if(Serial.available()){    
    while(Serial.available()){
      byte b = Serial.read();
      if(b == 0xaa){
        Serial.println("FORWARDED");
      }
      client.write(b);
    }    
  }
  if(client.available()){
    String s = "";
    while(client.available()){
      s += (char)client.read();
    }
    
    Serial.print(s);
    if(s.indexOf("ERNHD") >= 0 || s.indexOf("ERNDT") >= 0){
      // What to do if server states that no header or no data was sent
    }else if(s.indexOf("OK") == 0){
      // Transmission Sucessfully Sent
      Serial.println("Transmission Sent");
    }else if(s.indexOf("SERVERCHECK") == 0){
      lastServerCheck = millis();
      Serial.println("Server Check Refresh");
    }
    else{      
//      client.println("==OK");
    }

  }
}
