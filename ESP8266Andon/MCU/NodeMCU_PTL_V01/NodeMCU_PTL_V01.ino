#include <ESP8266WiFi.h>
#include <TM1637Display.h>

const int CLK = D6; //Set the CLK pin connection to the display
const int DIO = D5; //Set the DIO pin connection to the display

// WIFI
const char* ssid = "EXPC";     // SSID
const char* password = "12345678"; // Password

WiFiClient client;
const uint16_t port = 7888;
const char* host = "datditdut.com"; // IP serveur - Server IP

int totalPick;
int limitPick;
int lamp1 = D2;
int lamp2 = D0;
int PB_Hijau = D4;
int PB_MIN = D3;
int PB_ADD = D1;
int PB1_State = 0;
int PB1_LOW = 0;
int PB1_Status = 0;
int PB2_State = 0;
int PB2_LOW = 0;
int PB2_Status = 0;
int kirim = 0;
int nilai = 0;
int b1State = HIGH;
int b2State = HIGH;
int b3State = HIGH;
String ch;

short loader = 0;
long blinkTimer1 = 0;
bool blinkHijauEnable = false;

TM1637Display display(CLK, DIO); //set up the 4-Digit Display.
const uint8_t SEG_DONE[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
  };

const uint8_t SEG_CONN1[] = {
  SEG_A | SEG_F | SEG_E | SEG_D,
  SEG_E | SEG_G | SEG_C | SEG_D,
  SEG_C | SEG_E | SEG_G,
  SEG_A
};
const uint8_t SEG_CONN2[] = {
  SEG_A | SEG_F | SEG_E | SEG_D,
  SEG_E | SEG_G | SEG_C | SEG_D,
  SEG_C | SEG_E | SEG_G,
  SEG_B | SEG_C
};
const uint8_t SEG_CONN3[] = {
  SEG_A | SEG_F | SEG_E | SEG_D,
  SEG_E | SEG_G | SEG_C | SEG_D,
  SEG_C | SEG_E | SEG_G,
  SEG_D
};
const uint8_t SEG_CONN4[] = {
  SEG_A | SEG_F | SEG_E | SEG_D,
  SEG_E | SEG_G | SEG_C | SEG_D,
  SEG_C | SEG_E | SEG_G,
  SEG_E | SEG_F
};

const uint8_t SEG_CONN_OK[] = {
  SEG_A | SEG_F | SEG_G | SEG_C | SEG_D,
  SEG_F | SEG_E,
  SEG_F | SEG_E,
  SEG_A | SEG_F | SEG_B | SEG_E | SEG_G
};

const uint8_t SEG_MIN[] = {
  SEG_G,  // d
  SEG_G,  // O
  SEG_G,  // n
  SEG_G   // E
  };

void setup() {
  pinMode(lamp1, OUTPUT);
  pinMode(lamp2, OUTPUT);
  pinMode(PB_Hijau, INPUT);
  pinMode(PB_Hijau,INPUT_PULLUP);
  pinMode(PB_MIN,INPUT_PULLUP);
  pinMode(PB_MIN, INPUT);
//  pinMode(PB_ADD, INPUT_PULLUP);
  pinMode(PB_ADD, INPUT);

  display.setBrightness(0x0a); //set the diplay to maximum brightness
  display.setSegments(SEG_MIN);

  /** Trigger LOW*/
  digitalWrite(lamp1, HIGH);
  digitalWrite(lamp2, HIGH);

  // connect to WIFI
  Serial.begin(9600);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    if(loader == 0){
      display.setSegments(SEG_CONN1);
    }else if(loader == 1){
      display.setSegments(SEG_CONN2);
    }else if(loader == 2){
      display.setSegments(SEG_CONN3);
    }else if(loader == 3){
      display.setSegments(SEG_CONN4);
    }
    if(loader >= 3){
      loader = 0;
    }else{
      loader++;
    }
    Serial.print(".");
    delay (100);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  display.setSegments(SEG_CONN_OK);
  delay(2000);
  display.setSegments(SEG_MIN);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("MCA: ");
  Serial.println(WiFi.macAddress());

}

void loop()
{
  displayBlink();
  if (!client.connected())
  {
    Serial.println("Trying to connect to server " + String(host) + ":" + String(port));
    client.connect(host, port);
    client.println("HANDSHAKE::OK");
    client.println("MCA::" + String(WiFi.macAddress()));
    return;
  }
  else
  {
    //blinkLedConst();
  }

  // ---- listenSerial();---
  if(client.available()){
    String command = client.readStringUntil('\r');
    command.replace("\n","");
    Serial.println("CMD FWD " + command);
    if(command.indexOf("PICK") >= 0){      
      // XXXXPICKYYYY
      // 012345678901      
      totalPick = command.substring(8,12).toInt();      
      limitPick = totalPick;
      int matCountBefore = command.substring(0,4).toInt();
      Serial.println("Picking " + String(totalPick) + " FROM " + String(matCountBefore));
      nilai = 1;
//      hijaublink ();
      blinkHijauEnable = true;
      comp_pin_v(totalPick);
    }
    if(command.indexOf("STOR") >= 0){      
      // XXXXSTORYYYY
      // 012345678901      
      totalPick = command.substring(8,12).toInt();
      limitPick = totalPick;
      int matCountBefore = command.substring(0,4).toInt();       
      Serial.println("Storing " + String(totalPick) + " FROM " + String(matCountBefore));
      nilai = 1;
//      hijaublink ();
      blinkHijauEnable = true;
      comp_pin_v(totalPick);
    }
  }
  if (nilai == 1)
    {
     blinkHijauEnable = true;
     }
  if (nilai == 2)
    {
     digitalWrite(lamp1, LOW);
     }
   
  buttonListen();
  
  
}


void buttonListen()
{
  int btnState1 = digitalRead(PB_Hijau); // BTNPUSH1 = D7
  int btnState2 = digitalRead(PB_MIN);
  int btnState3 = digitalRead(PB_ADD);
  if (btnState1 != b1State && b1State == LOW)
  {
//    Serial.println("BTNP " + String(btnState1 == HIGH ? "H" : "L") + " - " + String(btnState2 == HIGH ? "H" : "L"));
//    client.println("PRESS-B1");
      client.println("DONE:"+String(totalPick));
      blinkHijauEnable = false;
      nilai = 0;
      digitalWrite(lamp1, HIGH);
      display.setSegments(SEG_DONE);
      delay(2000);
      display.setSegments(SEG_MIN);
      totalPick=0;
      limitPick=0;
  }
  if (btnState2 != b2State && b2State == LOW)
  {
    Serial.println("BTNP MINUS");
//    client.println("PRESS-B2");

    if(blinkHijauEnable){
      comp_pin_v(--totalPick);
    }
    
  }
  if (btnState3 != b3State && b3State == LOW)
  {
    Serial.println("BTNP PLUS");
//    client.println("PRESS-B2");

    if(blinkHijauEnable && totalPick < limitPick){
      comp_pin_v(++totalPick);
    }
    
  }

  b1State = btnState1;
  b2State = btnState2;
  b3State = btnState3;
}

int comp_pin_v(int V)
{
  display.showNumberDec(V);  
  return 0;
//if (V == 0) {  display.showNumberDec(0);}
//if (V == 1) {  display.showNumberDec(1);}
//if (V == 2) {  display.showNumberDec(2);}
//if (V == 3) {  display.showNumberDec(3);}
//if (V == 4) {  display.showNumberDec(4);}
//if (V == 5) {  display.showNumberDec(5);}
//if (V == 6) {  display.showNumberDec(6);}
//if (V == 7) {  display.showNumberDec(7);}
//if (V == 8) {  display.showNumberDec(8);}
//if (V == 9) {  display.showNumberDec(9);}
//if (V == 10) {  display.showNumberDec(10);}
//if (V == 11) {  display.showNumberDec(11);}
//if (V == 12) {  display.showNumberDec(12);}
//if (V == 13) {  display.showNumberDec(13);}
//if (V == 14) {  display.showNumberDec(14);}
//if (V == 15) {  display.showNumberDec(15);}
//if (V == 16) {  display.showNumberDec(16);}
//if (V == 17) {  display.showNumberDec(17);}
//if (V == 18) {  display.showNumberDec(18);}
//if (V == 19) {  display.showNumberDec(19);}
//if (V == 20) {  display.showNumberDec(20);}
//if (V == 21) {  display.showNumberDec(21);}
//if (V == 22) {  display.showNumberDec(22);}
//if (V == 23) {  display.showNumberDec(23);}
//if (V == 24) {  display.showNumberDec(24);}
//if (V == 25) {  display.showNumberDec(25);}
//if (V == 26) {  display.showNumberDec(26);}
//if (V == 27) {  display.showNumberDec(27);}
//if (V == 28) {  display.showNumberDec(28);}
//if (V == 29) {  display.showNumberDec(29);}
//if (V == 30) {  display.showNumberDec(30);}
//if (V == 31) {  display.showNumberDec(31);}
//if (V == 32) {  display.showNumberDec(32);}
//if (V == 33) {  display.showNumberDec(33);}
//if (V == 34) {  display.showNumberDec(34);}
//if (V == 35) {  display.showNumberDec(35);}
//if (V == 36) {  display.showNumberDec(36);}
//if (V == 37) {  display.showNumberDec(37);}
//if (V == 38) {  display.showNumberDec(38);}
//if (V == 39) {  display.showNumberDec(39);}
//if (V == 40) {  display.showNumberDec(40);}
//if (V == 41) {  display.showNumberDec(41);}
//if (V == 42) {  display.showNumberDec(42);}
//if (V == 43) {  display.showNumberDec(43);}
//if (V == 44) {  display.showNumberDec(44);}
//if (V == 45) {  display.showNumberDec(45);}
//if (V == 46) {  display.showNumberDec(46);}
//if (V == 47) {  display.showNumberDec(47);}
//if (V == 48) {  display.showNumberDec(48);}
//if (V == 49) {  display.showNumberDec(49);}
//if (V == 50) {  display.showNumberDec(50);}
//if (V == 51) {  display.showNumberDec(51);}
//if (V == 52) {  display.showNumberDec(52);}
//if (V == 53) {  display.showNumberDec(53);}
//if (V == 54) {  display.showNumberDec(54);}
//if (V == 55) {  display.showNumberDec(55);}
//if (V == 56) {  display.showNumberDec(56);}
//if (V == 57) {  display.showNumberDec(57);}
//if (V == 58) {  display.showNumberDec(58);}
//if (V == 59) {  display.showNumberDec(59);}
//if (V == 60) {  display.showNumberDec(60);}
//if (V == 61) {  display.showNumberDec(61);}
//if (V == 62) {  display.showNumberDec(62);}
//if (V == 63) {  display.showNumberDec(63);}
//if (V == 64) {  display.showNumberDec(64);}
//if (V == 65) {  display.showNumberDec(65);}
//if (V == 66) {  display.showNumberDec(66);}
//if (V == 67) {  display.showNumberDec(67);}
//if (V == 68) {  display.showNumberDec(68);}
//if (V == 69) {  display.showNumberDec(69);}
//if (V == 70) {  display.showNumberDec(70);}
//if (V == 71) {  display.showNumberDec(71);}
//if (V == 72) {  display.showNumberDec(72);}
//if (V == 73) {  display.showNumberDec(73);}
//if (V == 74) {  display.showNumberDec(74);}
//if (V == 75) {  display.showNumberDec(75);}
//if (V == 76) {  display.showNumberDec(76);}
//if (V == 77) {  display.showNumberDec(77);}
//if (V == 78) {  display.showNumberDec(78);}
//if (V == 79) {  display.showNumberDec(79);}
//if (V == 80) {  display.showNumberDec(80);}
//if (V == 91) {  display.showNumberDec(91);}
//if (V == 92) {  display.showNumberDec(92);}
//if (V == 93) {  display.showNumberDec(93);}
//if (V == 94) {  display.showNumberDec(94);}
//if (V == 95) {  display.showNumberDec(95);}
//if (V == 96) {  display.showNumberDec(96);}
//if (V == 97) {  display.showNumberDec(97);}
//if (V == 98) {  display.showNumberDec(98);}
//if (V == 99) {  display.showNumberDec(99);}
//if (V == 100) {  display.showNumberDec(100);}
}
void displayBlink(){
  long timerNow = millis();
  long delta = timerNow - blinkTimer1;
  if(delta < 500){
    if(blinkHijauEnable){
      digitalWrite(lamp1,LOW);
    }
  }
  else if(delta >= 500 && delta < 1000){
    if(blinkHijauEnable){
      digitalWrite(lamp1,HIGH);
    }
  }else if(delta >= 1000 && delta < 1500){
    if(blinkHijauEnable){
      digitalWrite(lamp1,LOW);
    }
  }else if(delta >= 1500 && delta < 2000){
    if(blinkHijauEnable){
      digitalWrite(lamp1,HIGH);
    }    
  }else{
    if(blinkHijauEnable){
      digitalWrite(lamp1,LOW);
    }   
    blinkTimer1 = timerNow;
  }
}
void hijaublink ()
{
//  digitalWrite(lamp1, HIGH);
//  delay(500);
//  digitalWrite(lamp1, LOW);
//  delay(500);
}
