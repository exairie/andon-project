#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "libs.h"
#include <SimpleTimer.h>
#include "OTA.h"

#define BTNPUSH1 D7
#define BTNPUSH2 D6
#define LEDACT D5
//#define ONEWIREBUS D8
int b1State = LOW, b2State = LOW;

const char *ssid = "DESKTOP-BT6KQ3U 4198";     // SSID
const char *password = "12345678"; // Password
const int port = 7888;

int state, tempTimerId, voltageTimerId, currentTimerId;
String host = "datditdut.com"; // IP serveur - Server IP

SimpleTimer timer;
WiFiClient client;
//OneWire oneWire(ONEWIREBUS);
//DallasTemperature tempSensor(&oneWire);

void blinkLedAct()
{
  digitalWrite(LEDACT, LOW);
  delay(50);
  digitalWrite(LEDACT, HIGH);
  delay(50);
  digitalWrite(LEDACT, LOW);
  delay(50);
  digitalWrite(LEDACT, HIGH);
  delay(50);
}
void blinkLedConst()
{
  digitalWrite(LEDACT, LOW);
}
void ledOff()
{
  digitalWrite(LEDACT, HIGH);
}
void setup()
{
  Serial.begin(9600);
  pinMode(BTNPUSH1, INPUT);
  pinMode(BTNPUSH2, INPUT);
  pinMode(LEDACT, OUTPUT);
  pinMode(D8, OUTPUT);  

  Serial.print("Connecting to ");
  Serial.println(ssid);

//  startupTemp(tempSensor);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    blinkLedAct();
    Serial.print(".");
  }

  blinkLedConst();

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  setupOTA();
}

void loop()
{
  timer.run();
  handleOTA();
  if (!client.connected())
  {
    timer.disable(tempTimerId);
    blinkLedAct();
    blinkLedAct();
    blinkLedAct();
    Serial.println("Trying to connect to server " + String(host) + ":" + String(port));
    client.connect(host, port);
    client.println("HANDSHAKE::OK");
    client.println("MCA::" + String(WiFi.macAddress()));
    Serial.println("Handshake sent");
    // Serial.println("Status " + client.connected() ? "CONNECTED" : "NOT CONNECTED");
    return;
  }
  else
  {
    blinkLedConst();
  }

  buttonListen();
  listenSerial();
//  if(Serial.available()){
//    String data = Serial.readStringUntil('\r');
//    client.print(data + "\r\n");
//  }
//  if(client.available()){
//    String data = client.readStringUntil('\r');
//    data.trim();
//    Serial.print("CLIENT RECV : ");
//    Serial.print(data + "\r\n");
//    client.flush();
//  }  
}
void listenSerial()
{
  if (Serial.available())
  {
    String command = Serial.readStringUntil('\n');
    if (command.indexOf('SETIP') >= 0)
    {
      /** SETIP:192.168.2.3 */
      command.replace("SETIP:", "");
      host = command;
      Serial.println("Changing host to " + host);
      client.connect(host, port);
      Serial.println("Disconnecting");
    }
  }
  if(client.available()){
    String command = client.readStringUntil('\r');
    command.replace("\n","");
    Serial.println("CMD FWD " + command);
    if(command.indexOf("PICK") >= 0){      
      // XXXXPICKYYYY
      // 012345678901      
      int totalPick = command.substring(8,11).toInt();
      int matCountBefore = command.substring(0,4).toInt();       
      Serial.println("Picking " + String(totalPick) + " FROM " + String(matCountBefore));
      digitalWrite(D8, HIGH);
    }
    if(command.indexOf("STOR") >= 0){      
      // XXXXSTORYYYY
      // 012345678901      
      int totalPick = command.substring(8,11).toInt();
      int matCountBefore = command.substring(0,4).toInt();       
      Serial.println("Storing " + String(totalPick) + " FROM " + String(matCountBefore));
      digitalWrite(D8, HIGH);
    }
  }
}
void buttonListen()
{
  int btnState1 = digitalRead(BTNPUSH1); // BTNPUSH1 = D7
  int btnState2 = digitalRead(BTNPUSH2);
  if (btnState1 != b1State && b1State == HIGH)
  {
//    Serial.println("BTNP " + String(btnState1 == HIGH ? "H" : "L") + " - " + String(btnState2 == HIGH ? "H" : "L"));
//    client.println("PRESS-B1");
      client.println("DONE");
      digitalWrite(D8, LOW);
  }
  if (btnState2 != b2State && b2State == HIGH)
  {
    Serial.println("BTNP " + String(btnState1 == HIGH ? "H" : "L") + " - " + String(btnState2 == HIGH ? "H" : "L"));
    client.println("PRESS-B2");
  }

  b1State = btnState1;
  b2State = btnState2;
}
