import React, { useEffect } from "react";
import Events, { EVENT_TEMP } from "../../context/EventEmitter";
import { Card, CardBody, CardHeader } from "reactstrap";
export default function Temperature() {
  const onEngStatusChange = s => {};
  useEffect(() => {
    Events.on(EVENT_TEMP, onEngStatusChange);
    return () => {
      Events.off(EVENT_TEMP, onEngStatusChange);
    };
  }, []);
  return (
    <Card>
      <CardHeader>Temperature</CardHeader>
      <CardBody style={{ position: "relative", height: 400 }}>
        <div
          class="progress"
          style={{
            position: "absolute",
            width: 100,
            height: 300,
            left: 0,
            top: 0,
            bottom: 30,
            right: 0,
            margin: "auto"
          }}
        >
          <div
            className="progress-bar bg-danger"
            role="progressbar"
            style={{
              height: "50%",
              width: "100%",
              position: "absolute",
              left: 0,
              right: 0,
              bottom: 0
            }}
            aria-valuenow="80"
            aria-valuemin="0"
            aria-valuemax="100"
          ></div>
        </div>
        82&deg;C
      </CardBody>
    </Card>
  );
}
