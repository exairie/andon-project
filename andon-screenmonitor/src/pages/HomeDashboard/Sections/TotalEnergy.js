import React from "react";

export default function TotalEnergy(props) {
  const { data } = props;
  const kwtotal = data ? data.findValue("EnergyJumlah") : 0;
  return (
    <div className="total-energy">
      <h4>Total Energi</h4>
      <h3 className="text-right">{kwtotal} KWh</h3>
    </div>
  );
}
