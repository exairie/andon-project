import React, { useState, useEffect, useRef } from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useQuery } from "react-apollo";
import moment from "moment";

const OPTIONS = {
  chart: {
    type: "spline"
  },
  title: {
    text: "Data Arus"
  },

  subtitle: {
    text: "Phase R, S, T"
  },

  yAxis: {
    title: {
      text: "Ampere"
    }
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle"
  },
  xAxis: {
    tickInterval: 1,
    type: "category"
  },
  plotOptions: {},

  series: [
    {
      name: "Phase R",
      color: "red",
      data: []
    },
    {
      name: "Phase S",
      color: "yellow",
      data: []
    },
    {
      name: "Phase T",
      color: "green",
      data: []
    }
  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
};

export default function CurrentTrend(props) {
  const [chartDataCurrentR, setChartDataCurrentR] = useState([]);
  const [chartDataCurrentS, setChartDataCurrentS] = useState([]);
  const [chartDataCurrentT, setChartDataCurrentT] = useState([]);
  const [chartLabels, setChartLabels] = useState([]);
  const [disableUpdate, setDisableUpdate] = useState(false);
  const chart = useRef(null);

  useEffect(() => {
    if (!props.realtimeData) return;
    const rtd = {};
    rtd.date = moment(props.realtimeData[0].createdAt);
    props.realtimeData.forEach(x => {
      rtd[x.parameter] = x.value;
    });
    setChartLabels(chartLabels.concat([rtd.date.format("HH:mm:ss")]));
    setChartDataCurrentR(chartDataCurrentR.concat([Number(rtd.CurrentR)]));
    setChartDataCurrentS(chartDataCurrentS.concat([Number(rtd.CurrentS)]));
    setChartDataCurrentT(chartDataCurrentT.concat([Number(rtd.CurrentT)]));

    return () => {};
  }, [props.realtimeData]);

  useEffect(() => {
    if (chart.current) {
      // console.log("updating chart " + chartDataCurrentR.length);

      chart.current.chart.xAxis[0].setCategories(chartLabels);
      chart.current.chart.series[0].setData(chartDataCurrentR);
      chart.current.chart.series[1].setData(chartDataCurrentS);
      chart.current.chart.series[2].setData(chartDataCurrentT, true);
    }
    return () => {};
  }, [chartDataCurrentT, chartDataCurrentR, chartDataCurrentS, chartLabels]);
  useEffect(() => {
    const sorted = props.data.sort((a, b) => a.date.diff(b.date, "second"));

    setChartLabels(sorted.map(x => x.date.format("HH:mm:ss")));
    setChartDataCurrentR(sorted.map(x => Number(x.CurrentR)));
    setChartDataCurrentS(sorted.map(x => Number(x.CurrentS)));
    setChartDataCurrentT(sorted.map(x => Number(x.CurrentT)));
    return () => {};
  }, [props.data]);
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <Card>
      <CardBody>
        <Row>
          <Col>
            <HighchartsReact
              ref={chart}
              highcharts={Highcharts}
              options={OPTIONS}
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
}
