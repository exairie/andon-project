import React, { useState, useEffect, useRef } from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useQuery } from "react-apollo";
import moment from "moment";

const OPTIONS = {
  chart: {
    type: "spline"
  },
  title: {
    text: "Data Temperature "
  },

  subtitle: {
    text: "Probe A dan B"
  },

  yAxis: {
    title: {
      text: "Temperature"
    }
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle"
  },
  xAxis: {
    tickInterval: 1,
    type: "category"
  },
  plotOptions: {},

  series: [
    {
      name: "Probe A",
      color: "red",
      data: []
    },
    {
      name: "Probe B",
      color: "yellow",
      data: []
    }
    // {
    //   name: "Phase T",
    //   color: "green",
    //   data: []
    // }
  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
};

export default function TemperatureTrend(props) {
  const [chartDataTempA, setChartDataTempA] = useState([]);
  const [chartDataTempB, setChartDataTempB] = useState([]);
  // const [chartDataVoltageT, setChartDataVoltageT] = useState([]);
  const [chartLabels, setChartLabels] = useState([]);
  const [disableUpdate, setDisableUpdate] = useState(false);
  const chart = useRef(null);

  useEffect(() => {
    if (!props.realtimeData) return;
    const rtd = {};
    rtd.date = moment(props.realtimeData[0].createdAt);
    props.realtimeData.forEach(x => {
      rtd[x.parameter] = x.value;
    });
    setChartLabels(chartLabels.concat([rtd.date.format("HH:mm:ss")]));
    setChartDataTempA(
      chartDataTempA.concat([Number(rtd.KenaikantemperatureA)])
    );
    setChartDataTempB(
      chartDataTempB.concat([Number(rtd.KenaikantemperatureB)])
    );
    // setChartDataVoltageT(chartDataVoltageT.concat([Number(rtd.VoltageT)]));

    return () => {};
  }, [props.realtimeData]);

  useEffect(() => {
    if (chart.current) {
      // console.log("updating chart " + chartDataTempA.length);

      chart.current.chart.xAxis[0].setCategories(chartLabels);
      chart.current.chart.series[0].setData(chartDataTempA);
      chart.current.chart.series[1].setData(chartDataTempB, true);
      // chart.current.chart.series[2].setData(chartDataVoltageT, );
    }
    return () => {};
  }, [chartDataTempA, chartDataTempB, chartLabels]);
  useEffect(() => {
    const sorted = props.data.sort((a, b) => a.date.diff(b.date, "second"));

    setChartLabels(sorted.map(x => x.date.format("HH:mm:ss")));
    setChartDataTempA(sorted.map(x => Number(x.KenaikantemperatureA)));
    setChartDataTempB(sorted.map(x => Number(x.KenaikantemperatureB)));
    // setChartDataVoltageT(sorted.map(x => Number(x.VoltageT)));
    return () => {};
  }, [props.data]);
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <Card>
      <CardBody>
        <Row>
          <Col>
            <HighchartsReact
              ref={chart}
              highcharts={Highcharts}
              options={OPTIONS}
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
}
