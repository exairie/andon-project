import React from "react";
import { useQuery } from "react-apollo";
import { useState } from "react";
import moment from "moment";
import { gql } from "apollo-boost";
import { useEffect } from "react";
import PhaseTrend from "./PhaseTrend";
import CurrentTrend from "./CurrentTrend";
import TemperatureTrend from "./TemperatureTrend";
import Apollo from "../../../context/Apollo";
const HistoryQuery = gql`
  query getHistory($start: DateTime, $end: DateTime, $mac: String) {
    UpdateHistory(start: $start, end: $end, mac: $mac) {
      id
      mac
      andon_id
      parameter
      value
      createdAt
      updatedAt
    }
  }
`;
export default function Trends(props) {
  const [startQuery, setStartQuery] = useState(moment().add(-1, "day"));
  const [endQuery, setEndQuery] = useState(moment());
  const [realtimeData, setRealtimeData] = useState(null);
  const [updateSwitch, setUpdateSwitch] = useState(false);
  const [history, setHistory] = useState([]);
  const [queryData, setQueryData] = useState([]);
  const { data, loading, error, refetch } = useQuery(HistoryQuery, {
    variables: {
      start: startQuery,
      end: endQuery,
      mac: props.device
    }
  });
  useEffect(() => {
    setRealtimeData(props.currentData);
    // Update Cache
    try {
      const histories = queryData;
      setQueryData(histories.concat(props.currentData.filter(x => x.id)));
    } catch (error) {
      console.log(error);
    }
    // refetch({
    //   start: startQuery,
    //   end: endQuery,
    //   mac: props.device
    // }).then(e => console.log("Refetch Down"));
    return () => {};
  }, [props.currentData]);

  useEffect(() => {
    if (props.device) {
      refetch({
        start: startQuery,
        end: endQuery,
        mac: props.device
      }).then(e => console.log("Refetch Down"));
    }
    return () => {};
  }, [props.device]);
  let count = 0;
  useEffect(() => {
    if (data && data.UpdateHistory) {
      setQueryData(data.UpdateHistory);
    }
    return () => {};
  }, [data]);
  useEffect(() => {
    // console.log(queryData);
    const historyData = [];
    const history = queryData.map(x => {
      const dt = moment(x.createdAt);
      const periode = dt.format("YYYYMMDDHHmm") + dt.format("ss").substr(0, 1);
      return {
        ...x,
        periode
      };
    });
    history.forEach(x => {
      const dt = moment(x.createdAt);
      const periode = dt.format("YYYYMMDDHHmm") + dt.format("ss").substr(0, 1);

      const search = historyData.filter(y => y.periode === periode);
      if (search.length > 0) {
        const nd = search[0];
        nd[x.parameter] = x.value;
      } else {
        const nd = {
          periode: periode
        };
        nd[x.parameter] = x.value;
        nd.date = dt;
        historyData.push(nd);
      }
    });
    historyData.sort((a, b) => a.date.diff(b.date, "seconds"));
    if (historyData.length > 30) {
      historyData.splice(0, historyData.length - 30);
      // console.log("Reducing chart data");
    }
    setHistory(historyData);
    // console.log("Histories Updated. Length = " + historyData.length);
    return () => {};
  }, [queryData]);
  return (
    <React.Fragment>
      <br />
      <div className="row">
        <div className="col-md-12">
          <PhaseTrend
            mac={props.device}
            data={history}
            realtimeData={realtimeData}
            switch={updateSwitch}
          />
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-md-12">
          <CurrentTrend
            mac={props.device}
            data={history}
            realtimeData={realtimeData}
            switch={updateSwitch}
          />
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-md-12">
          <TemperatureTrend
            mac={props.device}
            data={history}
            realtimeData={realtimeData}
            switch={updateSwitch}
          />
        </div>
      </div>
    </React.Fragment>
  );
}
