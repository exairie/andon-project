import React, { useState, useEffect, useRef } from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useQuery } from "react-apollo";
import moment from "moment";

const OPTIONS = {
  chart: {
    type: "spline"
  },
  title: {
    text: "Data Voltase"
  },

  subtitle: {
    text: "Phase R, S, T"
  },

  yAxis: {
    title: {
      text: "Voltase"
    }
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle"
  },
  xAxis: {
    tickInterval: 1,
    type: "category"
  },
  plotOptions: {},

  series: [
    {
      name: "Phase R",
      color: "red",
      data: []
    },
    {
      name: "Phase S",
      color: "yellow",
      data: []
    },
    {
      name: "Phase T",
      color: "green",
      data: []
    }
  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
};

export default function PhaseTrend(props) {
  const [chartDataVoltageR, setChartDataVoltageR] = useState([]);
  const [chartDataVoltageS, setChartDataVoltageS] = useState([]);
  const [chartDataVoltageT, setChartDataVoltageT] = useState([]);
  const [chartLabels, setChartLabels] = useState([]);
  const [disableUpdate, setDisableUpdate] = useState(false);
  const chart = useRef(null);

  useEffect(() => {
    if (!props.realtimeData) return;
    const rtd = {};
    rtd.date = moment(props.realtimeData[0].createdAt);
    props.realtimeData.forEach(x => {
      rtd[x.parameter] = x.value;
    });
    setChartLabels(chartLabels.concat([rtd.date.format("HH:mm:ss")]));
    setChartDataVoltageR(chartDataVoltageR.concat([Number(rtd.VoltageR)]));
    setChartDataVoltageS(chartDataVoltageS.concat([Number(rtd.VoltageS)]));
    setChartDataVoltageT(chartDataVoltageT.concat([Number(rtd.VoltageT)]));

    return () => {};
  }, [props.realtimeData]);

  useEffect(() => {
    if (chart.current) {
      // console.log("updating chart " + chartDataVoltageR.length);

      chart.current.chart.xAxis[0].setCategories(chartLabels);
      chart.current.chart.series[0].setData(chartDataVoltageR);
      chart.current.chart.series[1].setData(chartDataVoltageS);
      chart.current.chart.series[2].setData(chartDataVoltageT, true);
    }
    return () => {};
  }, [chartDataVoltageT, chartDataVoltageR, chartDataVoltageS, chartLabels]);
  useEffect(() => {
    // const sorted = props.data.sort((a, b) => a.date.diff(b.date, "second"));
    const sorted = props.data;
    setChartLabels(sorted.map(x => x.date.format("HH:mm:ss")));
    setChartDataVoltageR(sorted.map(x => Number(x.VoltageR)));
    setChartDataVoltageS(sorted.map(x => Number(x.VoltageS)));
    setChartDataVoltageT(sorted.map(x => Number(x.VoltageT)));
    return () => {};
  }, [props.data]);
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <Card>
      <CardBody>
        <Row>
          <Col>
            <HighchartsReact
              ref={chart}
              highcharts={Highcharts}
              options={OPTIONS}
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
}
