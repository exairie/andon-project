import React from "react";

export default function Frequency(props) {
  const { data } = props;
  const freq = data ? data.findValue("FrequencyJumlah") : 0;
  // console.log(data.findValue("FrequencyJumlah"));
  return (
    <div className="frequency">
      <h4>Frekuensi</h4>
      <h3 className="text-right">{freq} Hz</h3>
    </div>
  );
}
