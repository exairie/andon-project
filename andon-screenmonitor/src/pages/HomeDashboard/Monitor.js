import React from "react";
import LEDControl from "./Sections/LEDControl";

import "./Monitor.scss";
import Phase from "./Sections/Phase";
import TotalEnergy from "./Sections/TotalEnergy";
import Frequency from "./Sections/Frequency";
import { Subscription, Query } from "react-apollo";
import gql from "graphql-tag";
import DeviceSelector from "./DeviceSelector";
import moment from "moment";
import Trends from "./Sections/Trends";

var currentSubscriptionMacAddress = "";
const subscriptionQuery = gql`
  subscription DeviceSubscription($mac: String) {
    AndonUpdates(mac: $mac) {
      id
      mac
      andon_id
      parameter
      value
      createdAt
      updatedAt
    }
  }
`;
class Monitor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentData: null,
      dataLog: [],
      selectedDevice: null
    };
    this.onDeviceSelect = this.onDeviceSelect.bind(this);
  }
  onDeviceSelect(device) {
    if (!device) return;
    console.log(device);
    this.setState({
      selectedDevice: device.identification
    });
  }
  render() {
    const { selectedDevice, currentData } = this.state;
    const vtot = { parameter: "volt_tot", value: 0 };
    const amptot = { parameter: "curr_tot", value: 0 };

    if (currentData) {
      currentData.forEach((data, i) => {
        if (
          data.parameter == "volt_r" ||
          data.parameter == "volt_s" ||
          data.parameter == "volt_t"
        ) {
          vtot.value += Number(data.value);
        }
        if (
          data.parameter == "curr_r" ||
          data.parameter == "curr_s" ||
          data.parameter == "curr_t"
        ) {
          amptot.value += Number(data.value);
        }
      });

      currentData.push(vtot);
      currentData.push(amptot);
    }

    return (
      <div className="animated fadeIn">
        <DeviceSelector
          initialSelection={selectedDevice}
          onDeviceSelect={this.onDeviceSelect}
        />
        <Subscription
          subscription={subscriptionQuery}
          onSubscriptionData={({ client, subscriptionData: payload }) => {
            const {
              data: { AndonUpdates }
            } = payload;
            const { dataLog } = this.state;
            dataLog.push(AndonUpdates);
            this.setState({
              dataLog,
              currentData: AndonUpdates
            });
            window.update = AndonUpdates;
            // console.log(
            //   AndonUpdates.reduce(
            //     (prev, current) =>
            //       prev + current.parameter + " : " + current.value + "\n",
            //     ""
            //   )
            // );
            // console.log(AndonUpdates);
          }}
          variables={{ mac: this.state.selectedDevice }}
          shouldResubscribe={() => {
            const resub =
              this.state.selectedDevice != currentSubscriptionMacAddress;
            currentSubscriptionMacAddress = this.state.selectedDevice;
            console.log("Should Resubscribe? " + resub);
            return resub;
          }}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3">
              <LEDControl
                data={currentData}
                currentMac={this.state.selectedDevice}
                control={"LAMP1"}
                filter={"pb1_on"}
              />
            </div>
            <div className="col-md-3">
              <LEDControl
                data={currentData}
                currentMac={this.state.selectedDevice}
                control={"LAMP2"}
                filter={"pb2_on"}
              />
            </div>
            <div className="col-md-3">
              <LEDControl
                data={currentData}
                currentMac={this.state.selectedDevice}
                control={"LAMP3"}
                filter={"pb3_on"}
              />
            </div>
            <div className="col-md-3">
              <LEDControl
                data={currentData}
                currentMac={this.state.selectedDevice}
                control={"LAMP4"}
                filter={"pb4_on"}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-3">
              <Phase title="Phase R" filter="R" data={currentData} />
            </div>
            <div className="col-md-3">
              <Phase title="Phase S" filter="S" data={currentData} />
            </div>
            <div className="col-md-3">
              <Phase title="Phase T" filter="T" data={currentData} />
            </div>
            <div className="col-md-3">
              <Phase title="Total" filter="Jumlah" data={currentData} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <TotalEnergy data={currentData} />
            </div>
            <div className="col-md-6">
              <Frequency data={currentData} />
            </div>
          </div>
          <Trends device={selectedDevice} currentData={currentData} />

          <div className="clearfix"></div>
        </div>
      </div>
    );
  }
}

export default Monitor;
