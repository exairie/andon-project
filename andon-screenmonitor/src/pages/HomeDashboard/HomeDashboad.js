import React, { useState } from "react";
import { CardGroup, Row, Col, Card, CardHeader, CardBody } from "reactstrap";
import Widget02 from "../../views/Widgets/Widget02";
import Power from "./Power";
import VoltageTrends from "./VoltageTrends";
import DeviceSelector from "./DeviceSelector";
import gql from "graphql-tag";
import { useSubscription } from "react-apollo";
import Temperature from "./Temperature";
const subscriptionQuery = gql`
  subscription DeviceSubscription($mac: String) {
    AndonUpdates(identifier: $mac) {
      parameter
      value
    }
  }
`;
var lastMacFilter = "";
function HomeDashboard(props) {
  const [selectedDevice, setSelectedDevice] = useState(null);
  const [andonParameters, setAndonParameters] = useState({});
  const { error, data } = useSubscription(subscriptionQuery, {
    onSubscriptionData({ subscriptionData: { data } }) {
      const { AndonUpdates } = data;
      setAndonParameters({
        ...andonParameters,
        [AndonUpdates.parameter]: AndonUpdates.value
      });
    },
    variables: {
      mac: selectedDevice ? selectedDevice.identification : "n/a"
    },
    shouldResubscribe(options) {
      const {
        variables: { mac }
      } = options;
      // console.log(
      //   `${mac} >>> ${selectedDevice && selectedDevice.identification}`
      // );
      const shouldResub =
        selectedDevice && selectedDevice.identification !== lastMacFilter;
      // if (shouldResub) alert("RESUB!");
      lastMacFilter = selectedDevice ? selectedDevice.identification : "";
      return shouldResub;
    }
  });
  const onDeviceSelect = device => {
    setSelectedDevice(device);
    setAndonParameters({});
  };
  return (
    <div className="animated fadeIn">
      <Row>
        <DeviceSelector
          initialSelection={selectedDevice}
          onDeviceSelect={onDeviceSelect}
        />
        <Col md="12">
          <Card>
            <CardHeader>Input Monitor</CardHeader>
            <CardBody>
              <CardGroup className="mb-4">
                <Widget02
                  mainText="No Response"
                  variant="2"
                  icon="fa fa-exclamation-triangle"
                  color="warning"
                  header="Input 1"
                  value="25"
                />
                <Widget02
                  mainText="Online"
                  variant="2"
                  icon="fa fa-gears"
                  color="success"
                  header="Input 2"
                  value="25"
                />
                <Widget02
                  mainText="Offline"
                  variant="2"
                  icon="fa fa-gears"
                  color="danger"
                  header="Input 3"
                  value="25"
                />
                <Widget02
                  mainText="On"
                  variant="2"
                  icon="fa fa-chevron-up"
                  color="primary"
                  header="Input 1"
                  value="25"
                />
                <Widget02
                  mainText="Off"
                  variant="2"
                  icon="fa fa-chevron-down"
                  color="danger"
                  header="Input 2"
                  value="25"
                />
              </CardGroup>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>Power</CardHeader>
            <CardBody>
              <Power info={andonParameters} />
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <Row>
                <Col md="8">
                  <VoltageTrends />
                </Col>
                <Col md="4">
                  <Temperature info={andonParameters} />
                </Col>
              </Row>
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <h1>
                Temperature :{" "}
                {andonParameters.temp ? andonParameters.temp : "n/a"}
              </h1>
              {JSON.stringify(andonParameters)}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default HomeDashboard;
