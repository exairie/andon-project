import React, { useState, useEffect, useRef } from "react";
import Highcharts from "highcharts";
import More from "highcharts/highcharts-more";
import HighchartsReact from "highcharts-react-official";
import { Row, Col, Container, Card, CardHeader, CardBody } from "reactstrap";
import Events, { EVENT_ENG_STATUS } from "../../context/EventEmitter";
import moment from "moment";

const _chartOptions = {
  chart: { type: "spline" },
  title: {
    text: "Power Trends"
  },

  yAxis: {
    title: {
      text: "Usage"
    }
  },
  xAxis: {
    labels: {
      enabled: true
    }
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle"
  },

  plotOptions: {
    series: {
      label: {
        // connectorAllowed: false
      }
    }
  },

  series: [
    {
      name: "Voltage",
      data: []
    },
    {
      name: "Current (Amps)",
      data: []
    },
    {
      name: "Power",
      data: []
    }
  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
};
function VoltageTrends(props) {
  const [engineStatus, setEngineStatus] = useState({});
  const [chartOptions, setChartOptions] = useState(_chartOptions);
  const chartRef = useRef(null);
  const onEngStatusChange = status => {
    console.log("Updating engine status");
    setEngineStatus(status);
  };
  useEffect(() => {
    Events.on(EVENT_ENG_STATUS, onEngStatusChange);
    return () => {
      Events.off(EVENT_ENG_STATUS, onEngStatusChange);
    };
  }, []);

  useEffect(() => {
    if (!chartRef.current) return;
    let { chart } = chartRef.current;
    const time = moment();
    const millis = time.valueOf();
    const now = time.format("HH:mm:ss");
    if (!engineStatus.current || !engineStatus.voltage) return;

    chart.series[0].addPoint({ name: now, y: engineStatus.voltage, t: millis });
    chart.series[1].addPoint({ name: now, y: engineStatus.current, t: millis });
    chart.series[2].addPoint({
      name: now,
      y: engineStatus.current * engineStatus.voltage,
      t: millis
    });

    /** Clear data from last 30 data */
    let points = chart.series[0].points;
    if (points.length > 30) {
      chart.series[0].removePoint(0);
      chart.series[1].removePoint(0);
      chart.series[2].removePoint(0);
    }

    // chart.redraw(false);

    return () => {};
  }, [engineStatus]);

  return (
    <HighchartsReact
      ref={chartRef}
      highcharts={Highcharts}
      options={chartOptions}
    />
  );
}

export default VoltageTrends;
