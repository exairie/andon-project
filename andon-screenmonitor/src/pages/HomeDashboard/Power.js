import React, { useState, useRef, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import More from "highcharts/highcharts-more";
import Events, { EVENT_ENG_STATUS } from "../../context/EventEmitter";

More(Highcharts);

const _chartOptions = {
  chart: {
    type: "gauge",
    plotBorderWidth: 1,
    plotBackgroundColor: {
      // linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
      // stops: [[0, "#FFF4C6"], [0.3, "#FFFFFF"], [1, "#FFF4C6"]]
    },
    plotBackgroundImage: null,
    height: 200
  },

  title: {
    text: "Voltage, Current, Power and Energy"
  },

  pane: [
    {
      startAngle: -45,
      endAngle: 45,
      background: null,
      center: ["15.3%", "145%"],
      size: 300
    },
    {
      startAngle: -45,
      endAngle: 45,
      background: null,
      center: ["46.6%", "145%"],
      size: 300
    },
    {
      startAngle: -45,
      endAngle: 45,
      background: null,
      center: ["84.9%", "145%"],
      size: 300
    }
    // {
    //   startAngle: -45,
    //   endAngle: 45,
    //   background: null,
    //   center: ["87.5%", "145%"],
    //   size: 300
    // }
  ],

  tooltip: {
    enabled: false
  },

  yAxis: [
    {
      min: 0,
      max: 50,
      minorTickPosition: "outside",
      tickPosition: "outside",
      labels: {
        rotation: "auto",
        distance: 20
      },
      plotBands: [
        {
          from: 0,
          to: 6,
          color: "#C02316",
          innerRadius: "100%",
          outerRadius: "105%"
        }
      ],
      pane: 0,
      title: {
        text: "Voltage (V)",
        y: -40
      }
    },
    {
      min: 0,
      max: 10,
      minorTickPosition: "outside",
      tickPosition: "outside",
      labels: {
        rotation: "auto",
        distance: 20
      },
      plotBands: [
        {
          from: 0,
          to: 6,
          color: "#C02316",
          innerRadius: "100%",
          outerRadius: "105%"
        }
      ],
      pane: 1,
      title: {
        text: "Current (A)",
        y: -40
      }
    },
    {
      min: 0,
      max: 1000,
      minorTickPosition: "outside",
      tickPosition: "outside",
      labels: {
        rotation: "auto",
        distance: 20
      },
      plotBands: [
        {
          from: 0,
          to: 6,
          color: "#C02316",
          innerRadius: "100%",
          outerRadius: "105%"
        }
      ],
      pane: 2,
      title: {
        text: "Power (W)",
        y: -40
      }
    }
    // {
    //   min: 0,
    //   max: 100,
    //   minorTickPosition: "outside",
    //   tickPosition: "outside",
    //   labels: {
    //     rotation: "auto",
    //     distance: 20
    //   },
    //   plotBands: [
    //     {
    //       from: 0,
    //       to: 6,
    //       color: "#C02316",
    //       innerRadius: "100%",
    //       outerRadius: "105%"
    //     }
    //   ],
    //   pane: 3,
    //   title: {
    //     text: "Energy (kWh)",
    //     y: -40
    //   }
    // }
  ],

  plotOptions: {
    gauge: {
      dataLabels: {
        enabled: false
      },
      dial: {
        radius: "100%"
      }
    }
  },

  series: [
    {
      name: "Ampere",
      data: [-20],
      yAxis: 0
    },
    {
      name: "Voltage",
      data: [-20],
      yAxis: 1
    },
    {
      name: "Power",
      data: [-20],
      yAxis: 2
    }
    // {
    //   name: "Energy",
    //   data: [-20],
    //   yAxis: 3
    // }
  ]
};

function Power(props) {
  // TODO : Hook with redux state change
  useEffect(() => {
    console.log(props.info);
    if (props.info.volt) {
      setVoltage(Number(props.info.volt));
    }
    if (props.info.curr) {
      setCurrent(Number(props.info.curr));
    }
    return () => {};
  }, [props]);
  const [chartOptions, setChartOptions] = useState(_chartOptions);
  const [voltage, setVoltage] = useState(0);
  const [current, setCurrent] = useState(0);
  const [kws, setKws] = useState(0);
  const chart = useRef(null);
  const refresh = () => {
    if (!chart.current) return;
    const voltage = Math.random() * 100;
    const current = Math.random() * 10;

    setVoltage(voltage);
    setCurrent(current);
  };
  useEffect(() => {
    console.log(`Parameter changed`);
    chart.current.chart.series[0].setData([voltage]);
    chart.current.chart.series[1].setData([current]);
    chart.current.chart.series[2].setData([voltage * current]);

    Events.emit(EVENT_ENG_STATUS, {
      voltage,
      current
    });

    const akws = (voltage * current) / 1000;
    setKws(k => k + akws);
    console.log(akws);
  }, [voltage, current]);
  return (
    <Row>
      <Col md="9">
        <HighchartsReact
          ref={chart}
          highcharts={Highcharts}
          options={chartOptions}
        />
      </Col>
      <Col
        md="3"
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: 200
        }}
      >
        <h5 className="text-center">Energy Usage</h5>
        <h4 className="text-center">{kws.toFixed(0)} kW</h4>
      </Col>
    </Row>
  );
}

export default Power;
