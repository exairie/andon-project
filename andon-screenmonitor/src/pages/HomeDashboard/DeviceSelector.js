import React, { useState, useEffect } from "react";
import PropTypes, { oneOf, any } from "prop-types";
import { Col, Card, CardHeader, CardBody } from "reactstrap";
import { useQuery } from "react-apollo";
import gql from "graphql-tag";
const getDevice = gql`
  query GetDevice {
    AndonDevice {
      identification
    }
  }
`;
function DeviceSelector(props) {
  const { onDeviceSelect, initialSelection } = props;
  const [selectedDeviceIndex, setSelectedDeviceIndex] = useState(-1);
  const [selectedDevice, setSelectedDevice] = useState(null);
  const { loading, error, data } = useQuery(getDevice, { pollInterval: 10000 });
  useEffect(() => {
    onDeviceSelect(selectedDevice);
    return () => {};
  }, [selectedDevice]);
  if (error) console.log(error);
  return (
    <Col md="12">
      <Card>
        <CardHeader>Select Device</CardHeader>
        <CardBody>
          {loading && (
            <h5>
              <i className="fa fa-spin fa-spinner"></i> Loading...
            </h5>
          )}
          {error && <h6>An error occured! {error.message}</h6>}
          {data && (
            <div className="form-group">
              <label htmlFor="">Select Device MAC Address</label>
              <select
                name=""
                id=""
                value={selectedDeviceIndex}
                className="form-control"
                onChange={e => {
                  const { value } = e.target;
                  const index = Number(value);
                  if (index === -1) return;

                  const sel = data.AndonDevice[index];
                  setSelectedDevice(sel);
                  setSelectedDeviceIndex(index);
                }}
              >
                <option value="-1" disabled>
                  Select Device
                </option>
                {data.AndonDevice.map((x, i) => {
                  if (
                    selectedDevice &&
                    selectedDevice.identification === x.identification
                  ) {
                    return (
                      <option value={i}>{x.identification} (Selected)</option>
                    );
                  } else return <option value={i}>{x.identification}</option>;
                })}
              </select>
            </div>
          )}
        </CardBody>
      </Card>
    </Col>
  );
}

DeviceSelector.propTypes = {
  onDeviceSelect: PropTypes.func,
  initialSelection: PropTypes.any
};

export default DeviceSelector;
