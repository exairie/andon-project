import React, { useState } from "react";

export const MATLIST = [
  {
    name: "Material A",
    mac: "",
    currentQty: 0
  },
  {
    name: "Material B",
    mac: "",
    currentQty: 0
  },
  {
    name: "Material C",
    mac: "",
    currentQty: 0
  },
  {
    name: "Material D",
    mac: "",
    currentQty: 0
  },
  {
    name: "Material E",
    mac: "",
    currentQty: 0
  }
];

export default function MaterialList(props) {
  const [selectedIndex, setSelectedIndex] = useState(-1);
  return (
    <select
      name=""
      id=""
      className="form-control"
      value={selectedIndex}
      onChange={e => {
        setSelectedIndex(Number(e.target.value));
        if (props.onSelect) {
          props.onSelect(Number(e.target.value));
        }
      }}
    >
      <option value="-1" disabled>
        Choose Material
      </option>
      {MATLIST.map((x, i) => {
        return <option value={i}>{x.name}</option>;
      })}
      <option value="-99">Create New</option>
    </select>
  );
}
