import React, { useState } from "react";
import { useSubscription, useQuery } from "react-apollo";
import gql from "graphql-tag";
const AndonDeviceQuery = gql`
  query AndonQuery {
    AndonDevice {
      identification
    }
  }
`;
function AndonDeviceList(props) {
  const { data, error, loading } = useQuery(AndonDeviceQuery);
  const [selectedDeviceId, setselectedDeviceId] = useState(-1);
  if (loading) return <h5>Loading...</h5>;
  if (error) return <h5>{error.toString()}</h5>;
  const { AndonDevice } = data;
  return (
    <select
      className="form-control"
      name=""
      id=""
      value={selectedDeviceId}
      onChange={e => {
        setselectedDeviceId(e.target.value);
        if (typeof props.onSelect === "function") {
          props.onSelect(
            AndonDevice.filter(x => x.identification === e.target.value)[0]
          );
        }
      }}
    >
      <option value="-1" disabled>
        Select One
      </option>
      {AndonDevice.map(x => (
        <option value={x.identification}>{x.identification}</option>
      ))}
    </select>
  );
}

export default AndonDeviceList;
