import React from "react";

export default function MaterialTable(props) {
  return (
    <table className="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Material Name</th>
          <th>Device Address</th>
          <th>Current Quantity</th>
        </tr>
      </thead>
      <tbody>
        {props.materials.map(x => {
          return (
            <tr>
              <td>{x.name}</td>
              <td>{x.mac}</td>
              <td>{x.currentQty}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
