import React from "react";
import { Card, CardBody } from "reactstrap";
import gql from "graphql-tag";
import { Mutation, Subscription } from "react-apollo";
import AndonDeviceList from "./AndonDeviceList";
import MaterialList, { MATLIST } from "./MaterialSelection";
import MaterialTable from "./MaterialList";
var _mactarget = "";
const MutationQuery = gql`
  mutation sendCommand($to: String, $command: String) {
    pressButton(code: $command, mac: $to) {
      result
    }
  }
`;
const SubscriptionQuery = gql`
  subscription onAndonLogAdded($mac: String) {
    AndonLogs(mac: $mac) {
      log
      mac
    }
  }
`;
export default class WarehouseMonitor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newMatName: "",
      selectedMat: null,
      createNewMat: false,
      cmdtext: "",
      mactarget: "",
      logtext: "",
      logs: [],
      lot: 0,
      quantity: 0,
      waitingDoneSignal: false,
      waitingPosition: null
    };
  }
  sendCommand() {}
  checkForSignals({ log, mac }) {
    const {
      waitingDoneSignal,
      waitingPosition,
      selectedMat,
      quantity
    } = this.state;
    if (waitingDoneSignal && log.indexOf("DONE") >= 0) {
      if (waitingPosition === "PICK") {
        selectedMat.currentQty -= quantity;
      } else {
        selectedMat.currentQty += Number(quantity);
      }
      this.setState({
        quantity: 0,
        waitingDoneSignal: false,
        waitingPosition: null
      });
    }
  }
  render() {
    const {
      mactarget,
      cmdtext,
      logtext,
      lot,
      quantity,
      createNewMat,
      newMatName,
      selectedMat
    } = this.state;

    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <h3>Warehouse Module</h3>

            <Card>
              <CardBody>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="">Master Order</label>
                      <input
                        type="text"
                        value={"Order 1, Order 2, Order 3"}
                        className="form-control"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="">Material</label>
                      <MaterialList
                        onSelect={index => {
                          console.log(index == -99);
                          if (index == -99) {
                            this.setState({
                              selectedMat: null,
                              createNewMat: true,
                              mactarget: ""
                            });
                          } else {
                            const mat = MATLIST[index];
                            this.setState({
                              selectedMat: mat,
                              createNewMat: false,
                              mactarget: mat.mac
                            });
                          }
                        }}
                      />
                      <h4 style={{ marginTop: 10 }}>
                        {selectedMat && selectedMat.name}
                      </h4>
                    </div>
                    {createNewMat && (
                      <div className="form-group">
                        <label htmlFor="">Material Name</label>
                        <input
                          type="text"
                          className="form-control"
                          value={newMatName}
                          onChange={e => {
                            this.setState({
                              newMatName: e.target.value
                            });
                          }}
                        />
                      </div>
                    )}
                    {createNewMat && (
                      <button
                        className="btn btn-success"
                        onClick={e => {
                          const m = {
                            name: newMatName,
                            mac: "",
                            currentQty: 0
                          };
                          MATLIST.push(m);
                          this.setState({
                            selectedMat: m,
                            createNewMat: false
                          });
                        }}
                      >
                        Add Material
                      </button>
                    )}
                    {selectedMat && selectedMat.mac === "" && (
                      <div className="form-group">
                        <label htmlFor="">Select MAC Address</label>
                        <AndonDeviceList
                          onSelect={device => {
                            const uSelectedMat = selectedMat;
                            uSelectedMat.mac = device.identification;
                            this.setState({
                              mactarget: device.identification,
                              selectedMat: uSelectedMat
                            });
                          }}
                        ></AndonDeviceList>
                      </div>
                    )}
                    <div className="form-group">
                      <label htmlFor="">Lot</label>
                      <input
                        type="text"
                        value={lot}
                        onChange={e => {
                          this.setState({ lot: e.target.value });
                        }}
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="">Quantity</label>
                      <input
                        type="number"
                        value={quantity}
                        onChange={e => [
                          this.setState({
                            quantity: e.target.value
                          })
                        ]}
                        className="form-control"
                      />
                    </div>
                    {!this.state.waitingDoneSignal && (
                      <Mutation mutation={MutationQuery}>
                        {mutate => (
                          <div className="row">
                            <div className="col-md-6">
                              <button
                                onClick={e => {
                                  this.setState({
                                    waitingDoneSignal: true,
                                    waitingPosition: "PICK"
                                  });
                                  mutate({
                                    variables: {
                                      to: mactarget,
                                      command: `${selectedMat.currentQty
                                        .toString()
                                        .padStart(
                                          4,
                                          "0"
                                        )}PICK${quantity
                                        .toString()
                                        .padStart(4, "0")}`
                                    }
                                  });
                                }}
                                className="btn btn-danger"
                                style={{ width: "100%" }}
                              >
                                PICK
                              </button>
                            </div>
                            <div className="col-md-6">
                              <button
                                onClick={e => {
                                  this.setState({
                                    waitingDoneSignal: true,
                                    waitingPosition: "STOR"
                                  });
                                  mutate({
                                    variables: {
                                      to: mactarget,
                                      command: `${selectedMat.currentQty
                                        .toString()
                                        .padStart(
                                          4,
                                          "0"
                                        )}STOR${quantity
                                        .toString()
                                        .padStart(4, "0")}`
                                    }
                                  });
                                }}
                                className="btn btn-success"
                                style={{ width: "100%" }}
                              >
                                STORE
                              </button>
                            </div>
                          </div>
                        )}
                      </Mutation>
                    )}
                    {this.state.waitingDoneSignal && (
                      <div>
                        <React.Fragment>
                          <h5 className="text-center">
                            <i className="fa fa-spinner fa-spin"></i> Waiting
                            for DONE signal
                          </h5>
                          <h6 className="text-center">Transaction info</h6>
                          <table className="table">
                            <tr>
                              <td>Material Name</td>
                              <td>{selectedMat.name}</td>
                            </tr>
                            <tr>
                              <td>Device Address</td>
                              <td>{selectedMat.mac}</td>
                            </tr>
                            <tr>
                              <td>QTY Change</td>
                              <td>
                                {selectedMat.currentQty}{" "}
                                {this.state.waitingPosition == "PICK"
                                  ? " - "
                                  : " + "}{" "}
                                {quantity}
                              </td>
                            </tr>
                          </table>
                        </React.Fragment>
                      </div>
                    )}
                  </div>
                  <div className="col-md-6">
                    <MaterialTable materials={MATLIST} />
                    <Mutation mutation={MutationQuery}>
                      {mutate => {
                        return (
                          <React.Fragment>
                            <div className="form-group">
                              <label htmlFor="">Mac Target</label>
                              <input
                                className="form-control"
                                type="text"
                                value={mactarget}
                                onChange={e => {
                                  this.setState({
                                    mactarget: e.target.value
                                  });
                                }}
                              />
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Command</label>
                              <input
                                className="form-control"
                                type="text"
                                value={cmdtext}
                                onChange={e => {
                                  this.setState({
                                    cmdtext: e.target.value
                                  });
                                }}
                                onKeyPress={e => {
                                  if (e.which == 13) {
                                    mutate({
                                      variables: {
                                        to: mactarget,
                                        command: cmdtext + "\r\n"
                                      }
                                    });
                                    this.setState({
                                      cmdtext: ""
                                    });
                                  }
                                }}
                              />
                            </div>
                          </React.Fragment>
                        );
                      }}
                    </Mutation>
                    <h6>Logs</h6>
                    <div
                      className="logText"
                      style={{
                        height: "300px",
                        overflowY: "auto",
                        overflowX: "hidden"
                      }}
                    >
                      <Subscription
                        shouldResubscribe={({ variables: { mac } }) => {
                          console.log(`Resub ${mac} != ${mactarget}`);
                          var res = mac != _mactarget;
                          _mactarget = mactarget;
                          return res;
                        }}
                        onSubscriptionData={({
                          subscriptionData: {
                            data: { AndonLogs }
                          }
                        }) => {
                          this.setState(() => {
                            this.checkForSignals(AndonLogs);
                            var logs = this.state.logs;
                            logs.push(AndonLogs);
                            return {
                              logs
                            };
                          });
                        }}
                        subscription={SubscriptionQuery}
                        variables={{ mac: mactarget }}
                      >
                        {({ data, error }) => {
                          // console.log(data);
                          return this.state.logs.map(x => (
                            <span style={{ display: "block" }}>
                              <strong>[{x.mac}]</strong> {x.log}
                            </span>
                          ));
                        }}
                      </Subscription>
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}
