import React, { useState } from "react";
import { Card, CardBody, Row, Col, CardHeader, Collapse } from "reactstrap";
import Select from "react-select";
import ReactDatePicker from "react-datepicker";
import moment from "moment";
import SaveCSV from "save-csv";
import { useEffect } from "react";

export default function MonitoringSettings(props) {
  const [settingsOpen, setSettingsOpen] = useState(true);
  const [exportData, setExportData] = useState([]);
  const {
    selectedDevices,
    setSelectedDevices,
    setDeviceLimited,
    deviceLimited,
    Device,
    selectedParams,
    setSelectedParams,
    parameterLimited,
    setParameterLimited,
    Parameter,
    dateFrom,
    setDateFrom,
    dateTo,
    setDateTo,
    loadData,
    analyticsLoading,
    chartData
  } = props;
  const ExportCSV = () => {
    const exportData = [];
    for (let c of chartData) {
      for (let mcdata of c.data) {
        const cdata = {
          mac: mcdata.mac,
          date: c.date.format("DD MMM YYYY HH:mm:ss"),
          ...mcdata.values
        };
        exportData.push(cdata);
      }
    }
    console.log(exportData);
    SaveCSV(exportData, {
      filename: "ExportCSV.csv"
    });
  };
  return (
    <Card>
      <CardHeader>
        <h3>
          View Options{" "}
          <button
            className="btn btn-info pull-right text-white"
            onClick={e => setSettingsOpen(!settingsOpen)}
          >
            <i
              className={
                settingsOpen ? "fa fa-chevron-up" : "fa fa-chevron-down"
              }
            ></i>{" "}
            Settings
          </button>
        </h3>
      </CardHeader>

      <Collapse isOpen={settingsOpen}>
        <CardBody>
          <Row>
            <Col md="6">
              <h5 htmlFor="">Select Mac Addresses</h5>
              <Select
                isMulti
                yield
                value={selectedDevices}
                onChange={e => {
                  if ((e?.length ?? 0) < 3) {
                    setSelectedDevices(e ?? []);
                    setDeviceLimited(false);
                  } else {
                    setDeviceLimited(true);
                  }
                }}
                options={(Device?.AndonDevice ?? []).map(x => {
                  return {
                    label: x.identification,
                    value: x.identification,
                    raw: x
                  };
                })}
              />
              {deviceLimited && (
                <p className="text-danger">Device tidak bisa lebih dari 3!</p>
              )}
            </Col>
            <Col md="6">
              <h5 htmlFor="">Select Parameters</h5>
              <Select
                isMulti
                value={selectedParams}
                onChange={e => {
                  if ((e?.length ?? 0) <= 3) {
                    setSelectedParams(e ?? []);
                    setParameterLimited(false);
                  } else {
                    setParameterLimited(true);
                  }
                }}
                options={(Parameter?.AllParameters ?? []).map(x => {
                  return {
                    label: x.parameter,
                    value: x.parameter,
                    raw: x
                  };
                })}
              />
              {parameterLimited && (
                <p className="text-danger">
                  Parameter tidak bisa lebih dari 3!
                </p>
              )}
            </Col>
          </Row>
          <br />
          <Row>
            <Col md="3">
              <h6>Date From</h6>
              <ReactDatePicker
                wrapperClassName="expand100"
                className="form-control"
                selected={dateFrom.toDate()}
                onChange={date => setDateFrom(moment(date))}
                timeInputLabel="Time:"
                timeFormat="HH:mm"
                dateFormat="MM/dd/yyyy HH:mm"
                showTimeSelect
              />
            </Col>
            <Col md="3">
              <h6>Date To</h6>
              <ReactDatePicker
                wrapperClassName="expand100"
                className="form-control"
                selected={dateTo.toDate()}
                onChange={date => setDateTo(moment(date))}
                timeInputLabel="Time:"
                timeFormat="HH:mm"
                dateFormat="MM/dd/yyyy HH:mm"
                showTimeSelect
              />
            </Col>
            <Col md="6" className="text-right">
              <button className="btn btn-warning" onClick={_ => ExportCSV()}>
                <i className="fa fa-download"></i> CSV
              </button>
              &nbsp;
              <button
                onClick={e => loadData()}
                disabled={analyticsLoading}
                className="btn btn-success"
              >
                <i className="fa fa-search"></i> Search
              </button>
            </Col>
          </Row>
        </CardBody>
      </Collapse>
    </Card>
  );
}
