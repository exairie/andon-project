import React, { useState } from "react";
import Events from "../../context/EventEmitter";
import { Card, CardBody } from "reactstrap";
import moment from "moment";
import { useEffect } from "react";

export default function PointInfo(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const updateFunction = data => {
      setData(data);
    };
    Events.on("update_pointer", updateFunction);
    return () => {
      Events.off("update_pointer", updateFunction);
    };
  }, []);
  return (
    <Card>
      <CardBody>
        <h4>{props.filter}</h4>
        {data
          .filter(x => x.param == props.filter)
          .map(x => {
            return (
              <React.Fragment>
                <h6>{x.mac}</h6>
                <h5>{x.value.toFixed(2)}</h5>
                <small>{moment(x.time).format("DD MMM HH:mm:ss")}</small>
              </React.Fragment>
            );
          })}
      </CardBody>
    </Card>
  );
}
