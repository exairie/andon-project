import React, { useEffect, useState, createRef } from "react";

import {
  Row,
  Col,
  Container,
  Card,
  CardHeader,
  CardBody,
  Collapse
} from "reactstrap";
import HC from "highcharts";
import HighchartsReact from "highcharts-react-official";
import Select from "react-select";
import _ from "lodash";
import { gql } from "apollo-boost";
import { useQuery, useLazyQuery } from "react-apollo";
import moment from "moment";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import PointInfo from "./PointInfo";
import Events from "../../context/EventEmitter";
import MonitoringSettings from "./MonitoringSettings";

const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
console.log(timezone);

let refs = [];

const PopulateDate = data => {
  const Structure = [
    {
      date: moment(),
      data: [
        {
          mac: "",
          values: {
            Power: 30
          }
        }
      ]
    }
  ];

  const accumulated = [];

  data.forEach(x => {
    const date = moment(x.createdAt);
    const periode =
      date.format("YYYYMMDDHHmm") + date.format("ss").substr(0, 1);
    const value = isNaN(x.value) ? 0 : Number(x.value);
    let i = 0;
    const search = accumulated.filter(a => {
      return a.periode == periode;
    });
    if (search.length > 0) {
      const s = search[0];
      const searchMac = s.data.filter(y => y.mac == x.mac);
      if (searchMac.length > 0) {
        // Mac registered
        let deviceData = searchMac[0];
        deviceData.values[x.parameter] = value;
      } else {
        console.log(`Adding new value ${x.parameter} = ${value}`);
        if (i++ < 30) {
        }
        s.data.push({
          mac: x.mac,
          values: {
            [x.parameter]: value
          }
        });
      }
    } else {
      let s = {
        date: date,
        periode,
        data: [
          {
            mac: x.mac,
            values: {
              [x.parameter]: value
            }
          }
        ]
      };
      accumulated.push(s);
    }
  });
  console.log(accumulated);
  return accumulated;
};

const Highcharts = _(HC).clone();
const QueryParameter = gql`
  query {
    AllParameters {
      parameter
    }
  }
`;
const QueryAndon = gql`
  query {
    AndonDevice {
      id
      identification
      machine_number
      machine_code
      location
      block
      machine_name
      machine_description
      last_info_update
      createdAt
      updatedAt
    }
  }
`;
const QueryData = gql`
  query AnalyticQuery(
    $devices: [String]
    $parameters: [String]
    $from: DateTime
    $to: DateTime
  ) {
    GetAnalyticalUpdates(
      devices: $devices
      parameters: $parameters
      from: $from
      to: $to
    ) {
      id
      mac
      andon_id
      parameter
      value
      createdAt
      updatedAt
    }
  }
`;

Highcharts.Pointer.prototype.reset = function() {
  return undefined;
};

Highcharts.Point.prototype.highlight = function(event) {
  event = this.series.chart.pointer.normalize(event);
  this.onMouseOver(); // Show the hover marker
  this.series.chart.tooltip.refresh(this); // Show the tooltip
  this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
};
export default function MonitoringAnalytics(props) {
  const arrLength = 3;
  const [elRefs, setElRefs] = React.useState([]);
  const [settingsOpen, setSettingsOpen] = useState(true);
  const [dateFrom, setDateFrom] = useState(moment().add(-1, "hour"));
  const [dateTo, setDateTo] = useState(moment());
  const [deviceLimited, setDeviceLimited] = useState(false);
  const [parameterLimited, setParameterLimited] = useState(false);
  const [selectedDevices, setSelectedDevices] = useState([]);
  const [selectedParams, setSelectedParams] = useState([]);
  const [chartData, setChartData] = useState([]);
  const [pointData, setPointData] = useState([]);

  const {
    data: Analytics,
    loading: analyticsLoading,
    error: analyticsError,
    refetch: RefetchAnalytics
  } = useQuery(QueryData, {
    variables: {
      from: "",
      to: "",
      devices: "",
      parameters: ""
    }
  });

  const { data: Device, loading: deviceLoading, error: deviceError } = useQuery(
    QueryAndon
  );
  const {
    data: Parameter,
    loading: parameterLoading,
    error: parameterError
  } = useQuery(QueryParameter);

  function syncExtremes(e) {
    var thisChart = this.chart;

    if (e.trigger !== "syncExtremes") {
      // Prevent feedback loop
      elRefs.forEach(ref => {
        if (!ref.current) return;
        const chart = ref.current.chart;
        if (chart !== thisChart) {
          if (chart.xAxis[0].setExtremes) {
            // It is null while updating
            chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, {
              trigger: "syncExtremes"
            });
          }
        }
      });
    }
  }
  const ChartOptions = {
    colors: ["red", "blue", "green", "purple", "magenta", "cyan"],
    chart: {
      zoomType: "x",
      type: "spline",
      marginLeft: 40, // Keep all charts left aligned
      spacingTop: 20,
      spacingBottom: 20
    },
    time: {
      timezone: timezone,
      useUTC: false
    },
    plotOptions: {
      // area: {
      //   stacking: "normal",
      //   lineColor: "#666666",
      //   lineWidth: 1,
      //   marker: {
      //     lineWidth: 1,
      //     lineColor: "#666666"
      //   }
      // }
      series: {
        states: {
          inactive: {
            opacity: 1
          }
        }
      }
    },
    title: {
      text: "Title",
      align: "left",
      margin: 0,
      x: 30
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis: {
      labels: {
        format: "{value:%H:%M:%S}"
      },
      crosshair: true,
      events: {
        setExtremes: syncExtremes
      }
    },
    yAxis: {
      title: {
        text: null
      }
    },
    tooltip: {
      enabled: false,
      positioner: function() {
        return {
          // right aligned
          x: 0,
          y: 10 // align to title
        };
      },
      borderWidth: 0,
      backgroundColor: "none",
      pointFormat: "{point.y}",
      headerFormat: "",
      shadow: false,
      style: {
        fontSize: "18px"
      },
      valueDecimals: 1
      // split: true
    },
    series: []
  };

  useEffect(() => {
    if (Highcharts && Highcharts.charts)
      Highcharts.charts.forEach(c => {
        if (c) {
          c.reflow();
          console.log("Chart Reflows");
        }
      });
    ["mousemove", "touchmove", "touchstart"].forEach(function(eventType) {
      document
        .getElementById("chart-analytics-container")
        .addEventListener(eventType, touchMoveListener);
    });
    return () => {
      ["mousemove", "touchmove", "touchstart"].forEach(e => {
        document
          .getElementById("chart-analytics-container")
          .removeEventListener(e, touchMoveListener);
      });
    };
  }, []);
  useEffect(() => {
    // add or remove refs
    setElRefs(elRefs =>
      Array(arrLength)
        .fill()
        .map((_, i) => elRefs[i] || createRef())
    );
  }, [arrLength]);
  useEffect(() => {
    refs = elRefs;
    return () => {};
  }, [elRefs]);

  const loadData = () => {
    RefetchAnalytics({
      from: dateFrom,
      to: dateTo,
      devices: selectedDevices.map(x => x.value),
      parameters: selectedParams.map(x => x.value)
    }).then(console.log);
  };
  const touchMoveListener = e => {
    var chart, point, i, event;

    let pointData = [];
    for (i = 0; i < refs.length; i = i + 1) {
      chart = refs[i].current?.chart;
      if (!chart) continue;
      // Find coordinates within the chart
      event = chart.pointer.normalize(e);
      // Get the hovered point
      for (var serie of chart.series) {
        point = serie.searchPoint(event, true);
        if (point) {
          point.highlight(e);
          pointData.push({
            time: point.category,
            mac: point.series.name,
            param: point.series.chart.title.textStr,
            value: point.y
          });
        }
      }
    }
    Events.emit("update_pointer", pointData);
  };
  useEffect(() => {
    const data = PopulateDate(Analytics?.GetAnalyticalUpdates ?? []);
    setChartData(data);
    return () => {};
  }, [Analytics]);
  useEffect(() => {
    let data = [];
    selectedParams.forEach(({ value: parameter }, i) => {
      const prdata = {
        title: parameter,
        categories: [],
        series: []
      };
      chartData.forEach(d => {
        prdata.categories.push(d.date.toDate());
        d.data.forEach((deviceData, i) => {
          if (!prdata.series[i])
            prdata.series[i] = {
              mac: deviceData.mac,
              data: []
            };

          prdata.series[i].data.push(deviceData.values[parameter] ?? 0);
        });
      });
      data.push(prdata);
    });
    refs.forEach((ref, i) => {
      console.log(ref.current);
      if (!ref.current) return;
      let d = data[i];
      ref.current.chart.xAxis[0].setCategories(d.categories);
      ref.current.chart.setTitle({ text: d.title });
      d.series.forEach((serie, i) => {
        ref.current.chart.addSeries({
          name: d.series[i].mac,
          data: d.series[i].data
        });
      });
      console.log(ref.current.chart);
      ref.current.chart.redraw();
    });
    console.log(data);
    return () => {};
  }, [chartData, selectedParams]);
  console.log("Redraw");
  return (
    <div className="animated fadeIn">
      <Row>
        <Col md="12">
          <MonitoringSettings
            selectedDevices={selectedDevices}
            setSelectedDevices={setSelectedDevices}
            setDeviceLimited={setDeviceLimited}
            Device={Device}
            selectedParams={selectedParams}
            setSelectedParams={setSelectedParams}
            setParameterLimited={setParameterLimited}
            Parameter={Parameter}
            dateFrom={dateFrom}
            setDateFrom={setDateFrom}
            dateTo={dateTo}
            setDateTo={setDateTo}
            loadData={loadData}
            analyticsLoading={analyticsLoading}
            chartData={chartData}
          />
        </Col>
        <Col md="12" id="chart-analytics-container">
          {selectedParams.map(({ value, raw }, i) => {
            return (
              <Row>
                <Col md="10">
                  <HighchartsReact
                    ref={elRefs[i]}
                    highcharts={Highcharts}
                    options={ChartOptions}
                  />
                </Col>
                <Col md="2">
                  <PointInfo filter={value} />
                </Col>
              </Row>
            );
          })}
          {/* <HighchartsReact
            ref={elRefs[0]}
            highcharts={Highcharts}
            options={ChartOptions}
          />
          <HighchartsReact
            ref={elRefs[1]}
            highcharts={Highcharts}
            options={ChartOptions}
          />
          <HighchartsReact
            ref={elRefs[2]}
            highcharts={Highcharts}
            options={ChartOptions}
          /> */}
        </Col>
      </Row>
    </div>
  );
}
