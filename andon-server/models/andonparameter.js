"use strict";
module.exports = (sequelize, DataTypes) => {
  const AndonParameter = sequelize.define(
    "AndonParameter",
    {
      parameter: DataTypes.STRING
    },
    {}
  );
  AndonParameter.associate = function(models) {
    // associations can be defined here
  };
  return AndonParameter;
};
