'use strict';
module.exports = (sequelize, DataTypes) => {
  const AndonDevice = sequelize.define('AndonDevice', {
    identification: DataTypes.STRING,
    machine_number: DataTypes.INTEGER,
    machine_code: DataTypes.STRING,
    location: DataTypes.STRING,
    block: DataTypes.STRING,
    machine_name: DataTypes.STRING,
    machine_description: DataTypes.STRING,
    last_info_update: DataTypes.DATE
  }, {});
  AndonDevice.associate = function(models) {
    // associations can be defined here
  };
  return AndonDevice;
};