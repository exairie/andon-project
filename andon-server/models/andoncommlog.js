'use strict';
module.exports = (sequelize, DataTypes) => {
  const AndonCommLog = sequelize.define('AndonCommLog', {
    data: DataTypes.STRING,
    length: DataTypes.INTEGER,
    mac: DataTypes.STRING
  }, {});
  AndonCommLog.associate = function(models) {
    // associations can be defined here
  };
  return AndonCommLog;
};