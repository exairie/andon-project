'use strict';
module.exports = (sequelize, DataTypes) => {
  const AndonUpdateV2 = sequelize.define('AndonUpdateV2', {
    VoltageR: DataTypes.FLOAT,
    VoltageS: DataTypes.FLOAT,
    VoltageT: DataTypes.FLOAT,
    VoltageJumlah: DataTypes.FLOAT,
    CurrentR: DataTypes.FLOAT,
    CurrentS: DataTypes.FLOAT,
    CurrentT: DataTypes.FLOAT,
    CurrentJumlah: DataTypes.FLOAT,
    PowerR: DataTypes.FLOAT,
    PowerS: DataTypes.FLOAT,
    PowerT: DataTypes.FLOAT,
    PowerJumlah: DataTypes.FLOAT,
    EnergyJumlah: DataTypes.FLOAT,
    FrequencyJumlah: DataTypes.FLOAT,
    KenaikantemperatureA: DataTypes.FLOAT,
    KenaikantemperatureB: DataTypes.FLOAT,
    VibrationsensorA: DataTypes.FLOAT
  }, {});
  AndonUpdateV2.associate = function(models) {
    // associations can be defined here
  };
  return AndonUpdateV2;
};