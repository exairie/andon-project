"use strict";
module.exports = (sequelize, DataTypes) => {
  const AndonUpdate = sequelize.define(
    "AndonUpdate",
    {
      andon_id: DataTypes.INTEGER,
      parameter: DataTypes.STRING,
      value: DataTypes.STRING,
      mac: DataTypes.STRING
    },
    {}
  );
  AndonUpdate.associate = function(models) {
    // associations can be defined here
  };
  return AndonUpdate;
};
