'use strict';
module.exports = (sequelize, DataTypes) => {
  const AndonAlert = sequelize.define('AndonAlert', {
    andon_id: DataTypes.INTEGER,
    alert_code: DataTypes.STRING,
    alert_name: DataTypes.STRING,
    start_from: DataTypes.DATE,
    end_at: DataTypes.DATE,
    alert_duration: DataTypes.INTEGER,
    halt_process: DataTypes.BOOLEAN,
    urgency_level: DataTypes.INTEGER
  }, {});
  AndonAlert.associate = function(models) {
    // associations can be defined here
  };
  return AndonAlert;
};