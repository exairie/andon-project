import db from "../models";

export const checkDeviceRegistration = async d => {
  if (!d.mac) throw Error("Mac address not sent");
  let device = await db.AndonDevice.findOne({
    where: {
      identification: d.mac
    }
  });
  if (device) return device;
  else {
    return await db.AndonDevice.create({
      identification: d.mac,
      machine_number: "(unregistered)",
      machine_code: "(unregistered)",
      location: "",
      block: "",
      machine_name: "",
      machine_description: "",
      last_info_update: new Date()
    });
  }
};
