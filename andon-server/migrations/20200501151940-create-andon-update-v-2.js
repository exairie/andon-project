'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('AndonUpdateV2s', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      VoltageR: {
        type: Sequelize.FLOAT
      },
      VoltageS: {
        type: Sequelize.FLOAT
      },
      VoltageT: {
        type: Sequelize.FLOAT
      },
      VoltageJumlah: {
        type: Sequelize.FLOAT
      },
      CurrentR: {
        type: Sequelize.FLOAT
      },
      CurrentS: {
        type: Sequelize.FLOAT
      },
      CurrentT: {
        type: Sequelize.FLOAT
      },
      CurrentJumlah: {
        type: Sequelize.FLOAT
      },
      PowerR: {
        type: Sequelize.FLOAT
      },
      PowerS: {
        type: Sequelize.FLOAT
      },
      PowerT: {
        type: Sequelize.FLOAT
      },
      PowerJumlah: {
        type: Sequelize.FLOAT
      },
      EnergyJumlah: {
        type: Sequelize.FLOAT
      },
      FrequencyJumlah: {
        type: Sequelize.FLOAT
      },
      KenaikantemperatureA: {
        type: Sequelize.FLOAT
      },
      KenaikantemperatureB: {
        type: Sequelize.FLOAT
      },
      VibrationsensorA: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AndonUpdateV2s');
  }
};