'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('AndonAlerts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      andon_id: {
        type: Sequelize.INTEGER
      },
      alert_code: {
        type: Sequelize.STRING
      },
      alert_name: {
        type: Sequelize.STRING
      },
      start_from: {
        type: Sequelize.DATE
      },
      end_at: {
        type: Sequelize.DATE
      },
      alert_duration: {
        type: Sequelize.INTEGER
      },
      halt_process: {
        type: Sequelize.BOOLEAN
      },
      urgency_level: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AndonAlerts');
  }
};