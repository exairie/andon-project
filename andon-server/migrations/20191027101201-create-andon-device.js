'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('AndonDevices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      identification: {
        type: Sequelize.STRING
      },
      machine_number: {
        type: Sequelize.INTEGER
      },
      machine_code: {
        type: Sequelize.STRING
      },
      location: {
        type: Sequelize.STRING
      },
      block: {
        type: Sequelize.STRING
      },
      machine_name: {
        type: Sequelize.STRING
      },
      machine_description: {
        type: Sequelize.STRING
      },
      last_info_update: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AndonDevices');
  }
};