import { gql } from "apollo-server";
import GraphqlDateTime from "graphql-type-datetime";

const ScalarSchema = gql`
  scalar DateTime
`;
const ScalarResolver = {
  DateTime: GraphqlDateTime
};
export { ScalarSchema, ScalarResolver };
