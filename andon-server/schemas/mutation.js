import { gql } from "apollo-server";

const MutationSchema = gql`
  type Mutation {
    pressButton(mac: String, code: String): Response
  }
`;

export default MutationSchema;
