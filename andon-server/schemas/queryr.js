import { Op, QueryTypes } from "sequelize";
import moment from "moment";
const QueryResolver = {
  Query: {
    AndonDevice: async (_, __, { db, pub }) => {
      try {
        return await db.AndonDevice.findAll();
      } catch (error) {
        console.log(error);
        return error;
      }
    },
    AndonUpdates: async (_, __, { db, pub }) => {
      try {
        return await db.AndonUpdate.findAll();
      } catch (error) {
        console.log(error);
        return error;
      }
    },
    AllParameters: async (_, __, { db }) => {
      const query = "SELECT parameter from AndonParameters";
      return await db.sequelize.query(query, { type: QueryTypes.SELECT });
    },
    GetAnalyticalUpdates: async (
      _,
      { devices, parameters, from, to },
      { db }
    ) => {
      from = moment(from).format("YYYY-MM-DD HH:mm:ss");
      to = moment(to).format("YYYY-MM-DD HH:mm:ss");

      db.sequelize.options.logging = console.log;

      const data = await db.AndonUpdate.findAll({
        where: {
          mac: {
            [Op.in]: devices
          },
          parameter: {
            [Op.in]: parameters
          },
          createdAt: {
            [Op.between]: [from, to]
          }
        }
      });

      return data;
    },
    UpdateHistory: async (_, { start, end, mac }, { db }) => {
      const data = await db.AndonUpdate.findAll({
        where: {
          createdAt: {
            [Op.between]: [start, end]
          },
          mac
        },
        limit: 600,
        order: [["createdAt", "DESC"]]
      });

      return data;
    }
  }
};

export default QueryResolver;
