import { gql } from "apollo-server";

const QuerySchema = gql`
  type AndonDevices {
    id: Int
    identification: String
    machine_number: Int
    machine_code: String
    location: String
    block: String
    machine_name: String
    machine_description: String
    last_info_update: DateTime
    createdAt: DateTime
    updatedAt: DateTime
  }
  type AndonUpdates {
    id: Int
    mac: String
    andon_id: Int
    parameter: String
    value: String
    createdAt: DateTime
    updatedAt: DateTime
  }
  type LogInfo {
    log: String
    mac: String
  }
  type Parameter {
    parameter: String
  }
  type Query {
    AndonDevice: [AndonDevices]
    AndonUpdates: [AndonUpdates]
    UpdateHistory(start: DateTime, end: DateTime, mac: String): [AndonUpdates]
    AllParameters: [Parameter]
    GetAnalyticalUpdates(
      devices: [String]
      parameters: [String]
      from: DateTime
      to: DateTime
    ): [AndonUpdates]
  }
  type Response {
    result: String
    message: String
  }
`;

export default QuerySchema;
