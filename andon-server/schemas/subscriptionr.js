import { PubSub, withFilter } from "apollo-server";

const SubscriptionResolver = {
  Subscription: {
    AndonUpdates: {
      subscribe: withFilter(
        (_, __, { pub }) => pub.asyncIterator(["AndonUpdated"]),
        (payload, variable) => {
          console.log(variable);
          if (!variable.mac || variable.mac == "") return false;
          const parser = payload.mac == variable.mac;
          console.log(parser);
          return parser;
        }
      )
    },
    AndonLogs: {
      subscribe: withFilter(
        (_, __, { pub }) => pub.asyncIterator(["AndonLog"]),
        (payload, variable) => {
          var r = payload.AndonLogs.mac == variable.mac;
          console.log(
            `filtering payload ${payload.AndonLogs.mac} ${variable.mac} => ${r}`
          );
          return r;
        }
      )
    }
  }
};

export default SubscriptionResolver;
