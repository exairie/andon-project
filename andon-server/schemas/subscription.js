import { gql } from "apollo-server";

const SubscriptionSchema = gql`
  type Subscription {
    AndonUpdates(mac: String): [AndonUpdates]
    AndonLogs(mac: String): LogInfo
  }
`;

export default SubscriptionSchema;
