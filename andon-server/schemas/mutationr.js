import { TCPCommand } from "../socketlisteners/setup";

const MutationResolver = {
  Mutation: {
    pressButton(_, args, context, __) {
      console.log(`Button pressed with code ${args.code} to ${args.mac}`);
      TCPCommand({
        fwd: args.mac,
        command: args.code
      });
    }
  }
};

export default MutationResolver;
