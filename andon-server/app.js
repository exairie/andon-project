import Express from "express";
import SocketIO from "socket.io";
import Http from "http";
import InitSocketListeners from "./socketlisteners";
import { PubSub } from "apollo-server";
import { ApolloServer } from "apollo-server-express";
import db from "./models";
import QueryResolver from "./schemas/queryr";
import QuerySchema from "./schemas/query";
import MutationSchema from "./schemas/mutation";
import MutationResolver from "./schemas/mutationr";
import SubscriptionResolver from "./schemas/subscriptionr";
import SubscriptionSchema from "./schemas/subscription";
import { ScalarSchema, ScalarResolver } from "./schemas/scalar";
import { SubscriptionServer } from "subscriptions-transport-ws";
import { execute, subscribe } from "graphql";
import { Route } from "./routes";
import bodyParser from "body-parser";
import setupSocketIO from "./socketlisteners/setup";

export let startmode = "ALL";

if (process.env.STARTMODE) {
  startmode = process.env.STARTMODE;
}

const dir = "/home/admin/andonui";
const App = Express();

const http = Http.Server(App);

setupSocketIO();

App.use(bodyParser.json());

Route(App);

export const pub = new PubSub();

const Apollo = new ApolloServer({
  typeDefs: [ScalarSchema, QuerySchema, SubscriptionSchema, MutationSchema],
  context: async ({ req }) => {
    return {
      db,
      pub,
    };
  },
  resolvers: {
    ...QueryResolver,
    ...SubscriptionResolver,
    ...ScalarResolver,
    ...MutationResolver,
  },
  playground: {
    endpoint: "/graphql",
  },
});

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, "g"), replacement);
};

Apollo.installSubscriptionHandlers(http);
Apollo.applyMiddleware({ app: App });
// InitSocketListeners(IO);

App.use("/", Express.static(dir + "/front"));

App.use("/*", (req, res) => {
  res.sendFile(dir + "/front/index.html");
});

if (startmode == "ALL" || startmode == "SERVER") {
  let port = process.env.PORT || 9091;
  http.listen(port, () => {
    console.info("HTTP SERVER STARTED at port " + port);
    // new SubscriptionServer(
    //   {
    //     execute: execute,
    //     subscribe: subscribe,
    //     schema: [ScalarSchema, QuerySchema, SubscriptionSchema],
    //     resolvers: {
    //       ...QueryResolver,
    //       ...SubscriptionResolver,
    //       ...ScalarResolver
    //     }
    //   },
    //   {
    //     server: App,
    //     path: "/subscription"
    //   }
    // );
  });
}
