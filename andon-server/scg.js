const mysqlGQL = require("graphql-mysql-schema");

async function init() {
  const overrideRules = {
    datetime: "DateTime"
  };
  let r = await mysqlGQL(
    {
      host: "127.0.0.1",
      port: 3306,
      user: "root",
      password: "",
      database: "marva_andon"
    },
    {
      tableName: process.argv[2],
      genRule: overrideRules
    }
  );
  console.log(r);
}

init();
