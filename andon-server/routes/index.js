import InterfaceRouter from "./api";
import express from "express";

export const Route = app => {
  // app.use("/auth");
  // app.use("/administrator");
  app.use("/interface", InterfaceRouter);
};
