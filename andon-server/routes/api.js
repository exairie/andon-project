import Express from "express";
import { Interface } from "readline";
import db from "../models";
import { checkDeviceRegistration } from "../libs/Device";
import { pub } from "../app";

const InterfaceRouter = Express.Router();

InterfaceRouter.post("/update", async (req, res) => {
  try {
    const data = req.body;
    res.status(202).end();
    console.log(data);
    // console.log(db.AndonDevice.findOne);
    // return;
    /** Update data to database */
    for (let d of data) {
      let registration = await checkDeviceRegistration(d);
      // console.log(registration.id);
      const insert = [];
      const keys = [];
      const searchKey = [];
      for (let key of Object.keys(d)) {
        if (key === "mac") continue;
        let upd = db.AndonUpdate.create({
          mac: d.mac,
          andon_id: registration.id,
          parameter: key,
          value: d[key]
        });
        let paramSearch = db.AndonParameter.findOne({
          where: {
            parameter: key
          }
        });
        insert.push(upd);
        keys.push(key);
        searchKey.push(paramSearch);
        // db.AndonUpdate.findOne({
        //   where: {
        //     andon_id: registration.id,
        //     parameter: key
        //   }
        // }).then(async upd => {

        //   // if (upd) {
        //   //   upd.value = d[key];
        //   //   await upd.save();
        //   // } else {

        //   // }
        // });
      }
      const inserted = await Promise.all(insert);
      const params = await Promise.all(searchKey);
      // Update Parameter Values
      params.forEach((param, i) => {
        if (!param) {
          const parameterName = keys[i];
          db.AndonParameter.create({ parameter: parameterName })
            .then(e => {
              console.log(`Parameter Name ${parameterName} Created`);
            })
            .catch(console.log);
        }
      });

      console.log("Publishing Update");
      pub.publish("AndonUpdated", {
        mac: registration.identification,
        AndonUpdates: inserted
      });
    }
  } catch (error) {
    console.log(error);
    // res.status(500).json(error);
  }
});

export default InterfaceRouter;
