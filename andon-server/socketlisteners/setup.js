import Express from "express";
import SocketIO from "socket.io";
import Http from "http";
import { pub, startmode } from "../app";
import db from "../models";
import { Op } from "sequelize";

const App2 = Express();
const http2 = Http.Server(App2);
export var TCPENDPOINT = null;
export const IO = SocketIO(http2);
export function TCPCommand(payload) {
  if (!TCPENDPOINT) {
    console.warn("TCP ENDPOINT MISS");
    return false;
  }
  const payloadStr = JSON.stringify(payload);
  TCPENDPOINT.emit("command", payloadStr);
  return true;
}
export default function setupSocketIO() {
  IO.on("connection", (socket) => {
    console.log("Socket connected");
    socket.on("log", (data) => {
      try {
        data = data.replaceAll("\n", "");
        const dataJSON = JSON.parse(data);
        if (dataJSON.mac) {
          pub.publish("AndonLog", {
            AndonLogs: {
              mac: dataJSON.mac,
              log: dataJSON.log,
            },
          });
        }
      } catch (error) {
        console.error(data);
        console.log(error);
      }
    });
    socket.on("data", (data) => {
      console.log(`Socket sent ${data}`);
    });
    socket.on("tcpregister", () => {
      TCPENDPOINT = socket;
      console.log("TCP SERVER CONNECTED");
    });
    socket.on("log_comm", (data) => {
      db.AndonCommLog.create(data)
        .then((data) => {
          /** Delete comm log if data length is NOT 53 */
          db.AndonCommLog.destroy({
            where: {
              length: {
                [Op.not]: 53,
              },
            },
          })
            .then((e) => {})
            .catch(console.log);
        })
        .catch((e) => console.log(e));
    });
  });
  if (startmode == "ALL" || startmode == "SOCKET") {
    let port = process.env.SOCKETPORT || 9091;
    http2.listen(port, () => {
      console.info("Socket server start at port " + port);
    });
  }
}
