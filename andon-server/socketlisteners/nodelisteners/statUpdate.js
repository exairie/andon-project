import db from "../../models";
import { pub } from "../../app";

export default async function(data) {
  data = JSON.parse(data);
  // console.log(db.AndonDevice.findOne);
  // console.log(db);
  console.log(data);
  // return;
  /** Update data to database */
  for (let d of data) {
    let registration = await checkDeviceRegistration(d);
    for (let key of Object.keys(d)) {
      if (key === "mac") continue;
      db.AndonUpdate.upsert(
        {
          parameter: key,
          value: d[key]
        },
        {
          where: {
            andon_id: registration.id
          }
        }
      ).then(c => {
        console.log(`Andon ${registration.identification} - ${key} Updated`);
        // pub.publish("AndonUpdated", c);
      });
    }
  }
}

const checkDeviceRegistration = async d => {
  if (!d.mac) throw Error("Mac address not sent");
  let device = await db.AndonDevice.findOne({
    where: {
      identification: d.mac
    }
  });
  if (device) return device;
  else {
    return await db.AndonDevice.create({
      identification: d.mac,
      machine_number: "(unregistered)",
      machine_code: "(unregistered)",
      location: "",
      block: "",
      machine_name: "",
      machine_description: "",
      last_info_update: new Date()
    });
  }
};
