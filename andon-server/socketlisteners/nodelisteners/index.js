import fs from "fs";
const path = require("path");
const basename = path.basename(__filename);

const NodeListeners = socket => {
  fs.readdirSync(__dirname)
    .filter(file => {
      return (
        file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach(file => {
      let ev = file.replace(".js", "");
      console.log(`Installing IO events ${ev}`);
      let listener = require(path.join(__dirname, file)).default;
      //   console.log(typeof listener);
      socket.on(ev, listener);
    });
};

export default NodeListeners;
