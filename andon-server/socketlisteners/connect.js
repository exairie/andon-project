import NodeListeners from "./nodelisteners";
import fs from "fs";
const path = require("path");
const basename = path.basename(__filename);

export default function(socket) {
  console.log("Client connected");
  NodeListeners(socket);
}
