import Axios from "axios";
import { forwardCommand, tcpsockets } from "./sockets";
import { disableTelegram } from "..";

var lastChatId = -1;
var lastUpdate = 0;
var bufferMsg = "";
export function initTelegramPoll() {
  console.log("Initiating telegram update");
  Axios.get(
    "https://api.telegram.org/bot1009664415:AAF9KK4Vzbvv2VucYy7LigM75zwGguUAidM/getupdates"
  )
    .then(resp => {
      if (resp.status == 200) {
        const result = resp.data.result;
        for (let chat of result) {
          const id = Number(chat.update_id);
          if (id <= lastChatId) {
            // console.log(`SKIP ${id}`);
            continue;
          } else {
            if (chat.message && chat.message.text) {
              console.log(`Processing update id ${id} ${chat.message.text}`);
              const textSplit = chat.message.text.split(" ");
              const shortcode = textSplit[0];
              console.log(`SHORTCODE = ${shortcode}`);
              if (shortcode == "/cmd") {
                const mactarget = textSplit[1];
                const command = textSplit[2];

                invokeUpdateFromTelegram(mactarget, command);
                relayBackInfo(`Message ${command} Sent to ${mactarget}`);
              } else if (shortcode == "/list") {
                let str = tcpsockets.map(x => "MAC ADDR : " + x.mac).join("\n");
                str += "TCP Socket List : \n" + str;
                relayBackInfo(str);
                console.log(str);
              }
            }
            lastChatId = id;
          }
        }
      }
    })
    .catch(e => {
      console.log(e);
    });
}
function relayBackInfo(msg) {
  sendTelegram(msg);
}
function invokeUpdateFromTelegram(mac, command) {
  forwardCommand(mac, command);
}
export function sendTelegram(msg = null) {
  if (disableTelegram) return;
  if (msg && msg.indexOf("::") >= 0) {
    //wait until 10 update before send to server
    if (lastUpdate < 10) {
      bufferMsg += msg + "\n";
      lastUpdate++;
      return;
    } else {
      msg = bufferMsg + msg;
      lastUpdate = 0;
    }
  }
  Axios.post(
    "https://api.telegram.org/bot1009664415:AAF9KK4Vzbvv2VucYy7LigM75zwGguUAidM/sendmessage",
    {
      text: msg,
      chat_id: -361290455
    }
  )
    .then(resp => {
      if (resp.status == 200) {
        console.log("Telegram info sent!");
      }
    })
    .catch(e => {
      console.error(e);
    })
    .finally(() => {
      // lastUpdate = ctick;
    });
}
