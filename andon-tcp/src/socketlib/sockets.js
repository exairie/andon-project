import { serverUrl, io } from "..";
import Axios from "axios";
import { isInKeyword, keywords, initializeData } from "../keywordhandler";
export const tcpsockets = [];
import moment from "moment";

export function register(s) {
  if (checkExists(s)) {
    let prevsocket = null;
    for (let i in tcpsockets) {
      if (tcpsockets[i].mac === s.mac) {
        prevsocket = tcpsockets[i];
        unregister(prevsocket);
        break;
      }
    }
    if (typeof s.data == "undefined") {
      s.data = {};
    }
    if (prevsocket.data) {
      for (let k of Object.keys(prevsocket.data)) {
        if (isInKeyword(k)) {
          s.data[k] = prevsocket.data[k];
        }
      }
    }
  }
  console.log(`REGISTERING ${s.mac}`);

  s.lastUpdate = moment();
  tcpsockets.push(s);
  initializeData(s);
}
export function unregister(s) {
  console.log(`UNREGISTERING ${s.mac}`);
  let index = tcpsockets.indexOf(s);
  if (index < 0) return;
  tcpsockets.splice(index, 1);
}
function checkExists(s) {
  for (let is of tcpsockets) {
    if (is.mac == s.mac) return true;
  }
  return false;
}

export function forwardCommand(mac, command) {
  console.log(`Forwarding command ${command} to ${mac}`);
  for (var t of tcpsockets) {
    // console.log(`${t.mac === mac} ${t.mac}`);
    try {
      if (t.mac == mac) {
        const write = t.write(command + "\r\n", "utf8", () => {
          console.log(`[UP - ${mac}] ` + command);
        });
        console.log("Write " + write);
      }
    } catch (e) {
      console.log(e.message);
    }
  }
}

export function pingDevices() {
  return;
  /** SERVERCHECK */
  console.log(`Pinging Devices...`);
  tcpsockets.forEach(socket => {
    try {
      socket.write("SERVERCHECK\r\n");
    } catch (e) {
      console.error(e);
      /** Console log is redirected to monitor. Not to stdout */
      console.log(e);
    }
  });
  console.log(`Ping OK`);
}
export function updateToServer() {
  let data = tcpsockets.reduce((prev, cur) => {
    var obj = {};
    let data = cur.data;
    obj.mac = cur.mac;
    const lastUpdate = cur.lastUpdate;
    const timeLastUpdate = moment().diff(lastUpdate, "second");
    console.log(`Socket Last Update was ${timeLastUpdate} secs ago`);
    for (const k of Object.keys(data)) {
      obj[k.replace(/\s/g, "")] = data[k];
      // Reset all value if last update is more than 30 seconds ago
      if (timeLastUpdate > 5 * 60) {
        data[k] = 0;
      }
      // if (isInKeyword(k)) {
      // }
    }
    // cur.lastUpdate = moment();
    prev.push(obj);
    // console.log(obj);
    return prev;
  }, []);
  // console.log(data);
  // console.info("Sending data to central server");
  // io.emit("data", "UPDATE SENT");
  Axios.post(serverUrl + "/interface/update", data)
    .then(r => {
      // console.log(r);
      console.log(`Update response ${r.status}`);
    })
    .catch(e => {
      if (e.response) console.log(`Update response Error ${e.response.status}`);
    });
}
