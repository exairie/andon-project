import fs from "fs";
import Axios from "axios";
import { sendTelegram } from "../socketlib/telegram";
import { io, consoleLog } from "..";
import { tcpsockets, updateToServer } from "../socketlib/sockets";
import moment from "moment";
const path = require("path");
const basename = path.basename(__filename);
var socketinit = false;
let oldLogger = null;
let handlers = [];

const MonitoringData = () => ({
  VoltageR: 0,
  VoltageS: 0,
  VoltageT: 0,
  VoltageJumlah: 0,
  CurrentR: 0,
  CurrentS: 0,
  CurrentT: 0,
  CurrentJumlah: 0,
  PowerR: 0,
  PowerS: 0,
  PowerT: 0,
  PowerJumlah: 0,
  EnergyJumlah: 0,
  FrequencyJumlah: 0,
  KenaikantemperatureA: 0,
  KenaikantemperatureB: 0,
  VibrationsensorA: 0
});

const NotAllowZeroValue = [
  "VoltageR",
  "VoltageS",
  "VoltageT",
  "VoltageJumlah",
  "FrequencyJumlah",
  "KenaikantemperatureA",
  "KenaikantemperatureB"
];

const logHandler = {
  keyword: "FORWARDLOGS",
  handler: (data, s) => {
    try {
      s.write("Now all logging is forwarded. You are " + s.id + "\r\n");
      if (!oldLogger) {
        oldLogger = console.log;
      }
      console.log = str => {
        // consoleLog(str);
        try {
          s.write(str + "\r\n");
        } catch (e) {
          consoleLog(e);
        }
      };
    } catch (e) {
      console.error(e);
    }
  }
};
const phandler = {
  keyword: "PRINTALL",
  handler: (data, s) => {
    // console.log(tcpsockets);
    tcpsockets.forEach(sockets => {
      if (!sockets.mac) return;
      s.write(
        `\n- ${sockets.mac} U${sockets.lastUpdate.format(
          "DD MMM, HH:mm:ss"
        )} -\n`
      );
      const data = sockets.data;
      for (var key of Object.keys(data)) {
        s.write(`${key} : ${data[key]} \n`);
      }
      s.write(`-------- END ${sockets.mac} --------\r\n`);
    });
  }
};

handlers.push(phandler);
handlers.push(logHandler);

export var keywords = [];
export const isInKeyword = c => {
  return keywords.indexOf(c) >= 0;
};

export const initializeData = socket => {
  for (var keyword of keywords) {
    keyword = keyword.toLowerCase();
    if (keyword.indexOf("on") >= 0 || keyword.indexOf("off") >= 0) {
      keyword = keyword.replace(/_off|_off/, "");
      socket[keyword] = false;
    } else {
      socket[keyword] = 0;
    }
  }
};

const InitSocketListeners = () => {
  if (!handlers) handlers = [];
  fs.readdirSync(__dirname)
    .filter(file => {
      return (
        file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach(file => {
      let ev = file.replace(".js", "");
      let listener = require(path.join(__dirname, file)).default;
      keywords.push(ev.toLowerCase());
      handlers.push({
        keyword: ev,
        handler: listener
      });
    });
};
function ParseByteData(data, client) {
  if (data[data.length - 1] == 0xaa && data[0] == 0xff) {
    /** Check if header is exactly at 0 and trailer is exactly at end */
    console.log(`========== PARSING BYTE DATA ===========`);
    const offset = 1;
    const mData = MonitoringData();
    for (let i = 0; i < Object.keys(mData).length; i++) {
      const key = Object.keys(mData)[i];
      const mIndex = 3 * i;
      let byte1 = data[offset + mIndex],
        byte2 = data[offset + mIndex + 1],
        byte3 = data[offset + mIndex + 2];
      mData[key] = 0;
      mData[key] |= byte1 << 16;
      mData[key] |= byte2 << 8;
      mData[key] |= byte3;
      mData[key] = Number(mData[key]) / 100;

      /** if parameter does not allow 0 for value */
      if (NotAllowZeroValue.indexOf(key) >= 0) {
        /** then if its 0, revert back to previous value */
        /** But if previous value is exists */
        if (mData[key] == 0 && client.data && client.data[key]) {
          mData[key] == client.data[key] || 0;
        }
      }
    }
    if (typeof client.checkDataCounter == "undefined") {
      client.checkDataCounter = 0;
    }
    client.data = mData;
    client.lastUpdate = moment();
    console.log(client.data);
    client.countdownServerCheck = 1;
    console.log(`========== DONE ===========`);
  } else {
    console.log("UNKNOWN DATA FORMAT");
    console.log(data.map(x => x.toString(16)).join(" ") + ` : ${data.length}L`);
  }
}
function Handle(str, client) {
  if (!socketinit) {
    InitSocketListeners();
    socketinit = true;
  }
  sendTelegram(str);
  sendLog(str, client);
  str = str.trim();
  if (str.length < 1) return;
  var handled = false;

  /** Check for header */
  const headerPos = str.indexOf("==");
  if (headerPos < 0) {
    client.write("ERNHD\r\n");
    return;
  }

  str = str.substr(headerPos);

  const headerCheck = str.split("==");
  if (headerCheck.length < 1) {
    client.write("ERNDT\r\n");
    return;
  }

  console.log(`${client.mac}: ${str}`);
  str = str.replace("==", "");

  /** OLD MECH */
  let split = str.split("::");
  if (split.length > 1) {
    for (let h of handlers) {
      if (split[0] == h.keyword) {
        h.handler(split[1], client);
        handled = true;
      }
    }
  }
  /** NEW MECH */
  if (str.indexOf("::") < 0) {
    split = str.split(":").map(x => x.trim());

    if (split.indexOf("HANDSHAKE") >= 0) {
      client.write("HANDSHAKE OK\r\n");
      return;
    }
    client.lastUpdate = moment();
    try {
      if (typeof client.data == "undefined") {
        console.log("Truncating client data");
        client.data = {};
      }

      split[0] = split[0].replace(/\s/g, "");
      client.data[split[0]] = split[1];

      // const data = client.data;
      // for (var key of Object.keys(data)) {
      //   console.log(`${key} : ${data[key]}`);
      // }
      handled = true;
    } catch (e) {
      console.log(`Parameter error ${e}`);
    }
  }

  client.countdownServerCheck = 2;
  if (!handled) {
    console.log(`Handler for keyword ${str} not available`);
  } else {
    client.write("OK\r\n");
    // updateToServer();
  }
}

export function sendLog(str, client) {
  // console.log(`Emitting log ${str}`);
  // io.emit(
  //   "log",
  //   JSON.stringify({
  //     mac: client.mac,
  //     log: str
  //   })
  // );
}
export { ParseByteData };
export default Handle;

/**
 * 
  client.println("VOLT_R::" + String());
  client.println("VOLT_S::" + String());
  client.println("VOLT_T::" + String());
  client.println("VOLT_RS::" + String());
  client.println("VOLT_ST::" + String());
  client.println("VOLT_TR::" + String());

  client.println("CURR_R::" + String());
  client.println("CURR_S::" + String());
  client.println("CURR_T::" + String());

  client.println("KW_R::" + String());
  client.println("KW_S::" + String());
  client.println("KW_T::" + String());
  client.println("KW_TOT::" + String());

  client.println("KWH::" + String());
  client.println("FREQ::" + String());

  ----- ANA1 - 4  ini digunakan untuk baca data analog, nanti akan jadi temp, press ato yg lain------
  client.println("ANA1::" + String());
  client.println("ANA2::" + String());
  client.println("ANA3::" + String());
  client.println("ANA4::" + String());

  ----ini perintah tombol dari web untuk menyalakan lampu di arduino----
  client.println("PB1_ON");
  client.println("PB1_OFF");
  client.println("PB2_ON");
  client.println("PB2_OFF");
  client.println("PB3_ON");
  client.println("PB3_OFF");
  client.println("PB4_ON");
  client.println("PB4_OFF");

  ----- ini buat tampilkan status tombol yg dikonek ke arduino, tampilan di web berupa lampu saja ----------
  client.read("LAMP1_ON");
  client.read("LAMP1_OFF");
  client.read("LAMP2_ON");
  client.read("LAMP2_OFF");
  client.read("LAMP3_ON");
  client.read("LAMP3_OFF");
  client.read("LAMP4_ON");
  client.read("LAMP4_OFF");
 */
