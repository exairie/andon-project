import { register } from "../socketlib/sockets";

export default function(data, client) {
  // EC:FA:BC:28:5C:4B
  client.mac = data.substr(0, 17);
  console.log(`Registering client with mac address ${client.mac}`);
  register(client);
}
