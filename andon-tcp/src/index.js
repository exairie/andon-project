import net from "net";
import moment from "moment";
import IO from "socket.io-client";
import Handle, { ParseByteData } from "./keywordhandler";
import "./consoleinput";
import {
  updateToServer,
  forwardCommand,
  unregister,
  pingDevices
} from "./socketlib/sockets";
import { initTelegramPoll, sendTelegram } from "./socketlib/telegram";
export var disableTelegram = true;
if (process.env.TELEGRAM_DISABLED) {
  disableTelegram = true;
  console.log("Telegram Update is disabled");
}
export const serverUrl = "http://localhost:9090";
if (process.env.IO_ADDR) {
  port = process.env.IO_ADDR;
}

export var consoleLog = console.log;

export const io = IO("http://localhost:9091");
io.connect();
io.emit("data", "Hello From TCP");
setInterval(() => {
  io.emit("tcpregister");
}, 10000);
io.emit("tcpregister");
io.on("command", commandData => {
  const data = JSON.parse(commandData);
  /** key - SET */

  if (data.fwd) {
    forwardCommand(data.fwd, data.command);
    console.log("COMAND SENT! " + commandData);
  }
});

const server = net.createServer(c => {
  console.log("Client is connected ");
  c.on("error", err => {
    c.connected = false;
    c.destroy();
    consoleLog(err);
  });
  c.setKeepAlive(true, 0);
  c.on("ready", _ => {
    c.connected = true;
  });

  c.countdownServerCheck = 2;
  c.buffer = [];

  c.intervalId = setInterval(_ => {
    if (c.countdownServerCheck == 0) {
      try {
        console.log(`Replying Server Check for ${c.mac}`);
        c.write("SERVERCHECK\r\n");
      } catch (e) {
        console.log(e);
      }
      // after this, no server check
      c.countdownServerCheck = -1;
    } else if (c.countdownServerCheck > 0) {
      c.countdownServerCheck--;
    }
  }, 1000);

  c.on("data", data => {
    try {
      let strdata = "";
      // console.log("Data recv " + JSON.stringify(data));
      for (var i = 0; i < data.length; i++) {
        const charInput = data.toString().charAt(i);

        if (data[i] == 0xff) {
          /** Header Detected */

          c.buffer = [];
          c.buffer.push(data[i]);
        } else if (data[i] == 0xaa && c.buffer.length >= 52) {
          /** 52 EXCLUDES TRAILER */
          /** Trailer detected */
          c.buffer.push(data[i]);
          ParseByteData(c.buffer, c);
          io.emit("log_comm", {
            data: c.buffer.map(x => x.toString(16)).join(" "),
            length: c.buffer.length,
            mac: c.mac
          });
          // Handle(c.bufferText, c);

          /** Clear buffer */
          c.buffer = [];
          // c.write("CMD OK\r\n");
        } else {
          c.buffer.push(data[i]);
        }
      }
      if (data.toString().indexOf("::") >= 0) {
        Handle(data.toString(), c);
      }
      // Handle(data.toString(), c);
    } catch (error) {
      console.error(error);
    }
  });
  c.on("disconnect", () => {
    if (typeof c.intervalId != "undefined") {
      clearInterval(c.intervalId);
    }
    console.log(`Socket ${c.mac} UNREGISTER`);
    c.connected = false;
    unregister(c);
  });
  c.on("close", hadError => {
    console.log("Socket closed." + (hadError ? " it has errors!" : ""));
  });
});

server.listen(process.env.PORT || 7888, () => {
  console.log("SERVER INIT!");
  console.log(
    "Console won't be outputting anything, use FORWARDLOGS:: command to see logs"
  );
  // console.log = () => {};
});
setInterval(() => {
  pingDevices();
}, 10000);
setInterval(() => {
  updateToServer();
  if (!disableTelegram) {
    // initTelegramPoll();
    // sendTelegram();
  }
}, 1 * 60000);

export { server };
