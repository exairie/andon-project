import { forwardCommand } from "./socketlib/sockets";

// Get process.stdin as the standard input object.
var standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding("utf-8");

// Prompt user to input data in console.
console.log("Please input text in command line.");

// When user input data and click enter key.
standard_input.on("data", function(data) {
  // User input exit.
  if (data === "exit\n") {
    // Program exit.
    console.log("User input complete, program exit.");
    process.exit();
  } else {
    // Print user input in console.

    console.log("[INPUT:]" + data);
    if (data.indexOf("cmd") >= 0) {
      // CMD-[MAC]::message
      let split1 = data.split("-");
      let split2 = split1[1].split("::");
      let mac = split2[0];
      let command = split2[1];

      forwardCommand(mac, command.replace("\r", "").replace("\n", ""));
    }
  }
});
